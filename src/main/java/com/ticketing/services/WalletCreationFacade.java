/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.MyConstants;
import com.ticketing.rest.Users;
import com.ticketing.rest.WalletCreation;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Path;

/**
 *
 * @author pc
 */
@Stateless
@Path("walletcreation")
public class WalletCreationFacade extends AbstractFacade<WalletCreation> {

    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public WalletCreationFacade() {
        super(WalletCreation.class);
    }
    
    public void logWalletCreation(Users user){
        WalletCreation entity = new WalletCreation();
        entity.setUserId(user);
        entity.setCreatedAt(myCommon.myDate());
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        super.create(entity);
    }
}
