/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.EventsDisplayModel;
import com.ticketing.custom.EventsModel;
import com.ticketing.custom.MyConstants;
import com.ticketing.custom.MyResponse;
import com.ticketing.custom.MyStatus;
import com.ticketing.rest.Events;
import com.ticketing.rest.Users;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pc
 */
@Stateless
@Path("events")
public class EventsFacadeREST extends AbstractFacade<Events> {

    private static final Logger LOGGER = Logger.getLogger(Events.class.getName());
    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    public EventsFacadeREST() {
        super(Events.class);
    }
    @EJB
    UsersFacadeREST usersFacadeREST;
    @EJB
    EventTicketsFacadeREST eventTicketsFacadeREST;
    @EJB
    EventGalleryFacade eventGalleryFacade;
    @EJB
    EventSponsorFacade eventSponsorFacade;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createRecord(EventsModel eventsModel) {
        try {
            Users user = usersFacadeREST.find(eventsModel.getCreatedBy().getId());
            Date startTime = myCommon.timeFormat(eventsModel.getStartTime() + ":00");
            Date endTime = myCommon.timeFormat(eventsModel.getEndTime() + ":00");
            Date startDate = myCommon.strToDateConvert(eventsModel.getStartDate());
            Date endDate = myCommon.strToDateConvert(eventsModel.getEndDate());
            Date startDateTime = myCommon.strToDateTimeConvert(eventsModel.getStartDate() + " " + eventsModel.getStartTime() + ":00");
            Date endDateTime = myCommon.strToDateTimeConvert(eventsModel.getEndDate() + " " + eventsModel.getEndTime() + ":00");
            Events entity = new Events();
            entity.setCountryId(eventsModel.getCountryId());
            entity.setCountyId(eventsModel.getCountyId());
            entity.setCreatedAt(myCommon.myDate());
            entity.setCreatedBy(user);
            entity.setDescription(eventsModel.getDescription());
            entity.setEndDate(endDate);
            entity.setEndDateTime(endDateTime);
            entity.setEndTime(endTime);
            entity.setEventTypeId(eventsModel.getEventTypeId());
            entity.setLocation(eventsModel.getLocation());
            entity.setMerchantId(user.getMerchantId());
            entity.setName(eventsModel.getName());
            entity.setPoster(eventsModel.getPoster());
            entity.setStartDate(startDate);
            entity.setStartDateTime(startDateTime);
            entity.setStartTime(startTime);
            entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
            entity.setVenue(eventsModel.getVenue());
            entity.setLatitude(eventsModel.getLatitude());
            entity.setLongitude(eventsModel.getLongitude());
            super.create(entity);
            //log event creation communication
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Creation {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editRecord(EventsModel eventsModel) {
        try {
            Users user = usersFacadeREST.find(eventsModel.getCreatedBy().getId());
            Date startTime = myCommon.timeFormat(eventsModel.getStartTime() + ":00");
            Date endTime = myCommon.timeFormat(eventsModel.getEndTime() + ":00");
            Date startDate = myCommon.strToDateConvert(eventsModel.getStartDate());
            Date endDate = myCommon.strToDateConvert(eventsModel.getEndDate());
            Date startDateTime = myCommon.strToDateTimeConvert(eventsModel.getStartDate() + " " + eventsModel.getStartTime() + ":00");
            Date endDateTime = myCommon.strToDateTimeConvert(eventsModel.getEndDate() + " " + eventsModel.getEndTime() + ":00");
            
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Events> cq = cb.createQuery(Events.class);
            Root<Events> root = cq.from(Events.class);
            cq.where(cb.and(cb.equal(root.get("id"), eventsModel.getId()), cb.equal(root.get("merchantId"), user.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            Events entity = (Events) q.getSingleResult();
            
            entity.setCountryId(eventsModel.getCountryId());
            entity.setCountyId(eventsModel.getCountyId());
            entity.setDescription(eventsModel.getDescription());
            entity.setEndDate(endDate);
            entity.setEndDateTime(endDateTime);
            entity.setEndTime(endTime);
            entity.setEventTypeId(eventsModel.getEventTypeId());
            entity.setLocation(eventsModel.getLocation());
            entity.setName(eventsModel.getName());
            entity.setPoster(eventsModel.getPoster());
            entity.setStartDate(startDate);
            entity.setStartDateTime(startDateTime);
            entity.setStartTime(startTime);
            entity.setVenue(eventsModel.getVenue());
            entity.setLatitude(eventsModel.getLatitude());
            entity.setLongitude(eventsModel.getLongitude());
            super.edit(entity);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Updating {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteRecord(Events entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Events> cq = cb.createQuery(Events.class);
            Root<Events> root = cq.from(Events.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), user.getMerchantId()), cb.or(cb.equal(root.get("statusId"), MyConstants.STATUS_ACTIVE), cb.equal(root.get("statusId"), MyConstants.STATUS_PENDING_PUBLICATION), cb.equal(root.get("statusId"), MyConstants.STATUS_PENDING))));
            Query q = getEntityManager().createQuery(cq);
            Events events = (Events) q.getSingleResult();
            events.setStatusId(myCommon.getStatus(MyConstants.STATUS_DELETED));
            super.edit(events);
            //perform the cancellation procedure for the event subscribers

            //log cancellation communication
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Publication {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @PUT
    @Path("complete")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response completeRecord(Events entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Events> cq = cb.createQuery(Events.class);
            Root<Events> root = cq.from(Events.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), user.getMerchantId()), cb.equal(root.get("statusId"), MyConstants.STATUS_PENDING)));
            Query q = getEntityManager().createQuery(cq);
            Events events = (Events) q.getSingleResult();
            events.setStatusId(myCommon.getStatus(MyConstants.STATUS_ACTIVE));
            super.edit(events);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Creation Completeion {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @PUT
    @Path("authorize")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response authorizeRecord(Events entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Events> cq = cb.createQuery(Events.class);
            Root<Events> root = cq.from(Events.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), user.getMerchantId()), cb.equal(root.get("statusId"), MyConstants.STATUS_ACTIVE)));
            Query q = getEntityManager().createQuery(cq);
            Events events = (Events) q.getSingleResult();
            events.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING_PUBLICATION));
            super.edit(events);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Authorization {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @PUT
    @Path("publish")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response publishRecord(Events entity) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Events> cq = cb.createQuery(Events.class);
            Root<Events> root = cq.from(Events.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("statusId"), MyConstants.STATUS_PENDING_PUBLICATION)));
            Query q = getEntityManager().createQuery(cq);
            Events events = (Events) q.getSingleResult();
            events.setStatusId(myCommon.getStatus(MyConstants.STATUS_PUBLISHED));
            events.setCommission(entity.getCommission());
            super.edit(events);
            //log publication email
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Publication {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @PUT
    @Path("cancel")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response canceEvent(Events entity) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Events> cq = cb.createQuery(Events.class);
            Root<Events> root = cq.from(Events.class);
            cq.where(cb.equal(root.get("id"), entity.getId()));
            Query q = getEntityManager().createQuery(cq);
            Events events = (Events) q.getSingleResult();
            events.setStatusId(myCommon.getStatus(MyConstants.STATUS_CANCELLED));
            events.setRemarks(entity.getRemarks());
            super.edit(events);
            //log cancellation email
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Cancellation {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Events> getEvents(@QueryParam("statusId") String statusId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Events> cq = cb.createQuery(Events.class);
        Root<Events> root = cq.from(Events.class);
        if (statusId.isEmpty()) {
            cq.where(cb.and(cb.notEqual(root.get("statusId"), MyConstants.STATUS_PENDING), cb.notEqual(root.get("statusId"), MyConstants.STATUS_ACTIVE), cb.notEqual(root.get("statusId"), MyConstants.STATUS_DELETED)));
        } else {
            cq.where(cb.equal(root.get("statusId"), Integer.parseInt(statusId)));
        }
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    @GET
    @Path("{id}/{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findOne(@PathParam("id") Integer id, @PathParam("userId") Integer userId) {
        try {
            Users user = usersFacadeREST.find(userId);
            return Response.ok(this.getSingle(id, user.getMerchantId().getId())).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Events> getAll(@PathParam("userId") Integer userId, @QueryParam("statusId") String statusId) {
        Users user = usersFacadeREST.find(userId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Events> cq = cb.createQuery(Events.class);
        Root<Events> root = cq.from(Events.class);
        if (statusId.isEmpty()) {
            cq.where(cb.and(cb.notEqual(root.get("statusId"), MyConstants.STATUS_PENDING), cb.notEqual(root.get("statusId"), MyConstants.STATUS_DELETED), cb.equal(root.get("merchantId"), user.getMerchantId())));
        } else {
            cq.where(cb.and(cb.equal(root.get("statusId"), Integer.parseInt(statusId)), cb.equal(root.get("merchantId"), user.getMerchantId())));
        }
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    @GET
    @Path("pending/{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Events> getPendingEvent(@PathParam("userId") Integer userId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Events> cq = cb.createQuery(Events.class);
        Root<Events> root = cq.from(Events.class);
        cq.where(cb.and(cb.equal(root.get("statusId"), MyConstants.STATUS_PENDING), cb.equal(root.get("createdBy"), userId)));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    @GET
    @Path("active")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Events> getActiveEvent() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Events> cq = cb.createQuery(Events.class);
        Root<Events> root = cq.from(Events.class);
        cq.where(cb.and(cb.equal(root.get("statusId"), MyConstants.STATUS_PUBLISHED),cb.lessThanOrEqualTo(root.<Date>get("startDateTime"), myCommon.myDate()), cb.greaterThanOrEqualTo(root.<Date>get("endDateTime"), myCommon.myDate())));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    @GET
    @Path("comprehensive/{id}/{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public EventsDisplayModel getOne(@PathParam("id") Integer id, @PathParam("userId") Integer userId) {
        EventsDisplayModel eventsDisplayModel = new EventsDisplayModel();
        Users user = usersFacadeREST.find(userId);
        eventsDisplayModel.setEvent(this.getSingle(id, user.getMerchantId().getId()));
        eventsDisplayModel.setTickets(eventTicketsFacadeREST.getEventTickets(id, user.getMerchantId().getId(), String.valueOf(MyConstants.STATUS_ACTIVE)));
        eventsDisplayModel.setSponsors(eventSponsorFacade.getEventSponsors(id, user.getMerchantId().getId(), String.valueOf(MyConstants.STATUS_ACTIVE)));
        eventsDisplayModel.setGallery(eventGalleryFacade.getEventGallery(id, user.getMerchantId().getId(), String.valueOf(MyConstants.STATUS_ACTIVE)));
        return eventsDisplayModel;
    }

    @GET
    @Path("comprehensive/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public EventsDisplayModel getComprehensive(@PathParam("id") Integer id) {
        Events event = super.find(id);
        EventsDisplayModel eventsDisplayModel = new EventsDisplayModel();
        eventsDisplayModel.setEvent(event);
        eventsDisplayModel.setTickets(eventTicketsFacadeREST.getEventTickets(id, event.getMerchantId().getId(), String.valueOf(MyConstants.STATUS_ACTIVE)));
        eventsDisplayModel.setSponsors(eventSponsorFacade.getEventSponsors(id, event.getMerchantId().getId(), String.valueOf(MyConstants.STATUS_ACTIVE)));
        eventsDisplayModel.setGallery(eventGalleryFacade.getEventGallery(id, event.getMerchantId().getId(), String.valueOf(MyConstants.STATUS_ACTIVE)));
        return eventsDisplayModel;
    }

    //function to get single event
    public Events getSingle(int id, int merchantId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Events> cq = cb.createQuery(Events.class);
        Root<Events> root = cq.from(Events.class);
        cq.where(cb.and(cb.equal(root.get("id"), id), cb.equal(root.get("merchantId"), merchantId)));
        Query q = getEntityManager().createQuery(cq);
        return (Events) q.getSingleResult();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
