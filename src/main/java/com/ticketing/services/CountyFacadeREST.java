/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.rest.County;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author pc
 */
@Stateless
@Path("county")
public class CountyFacadeREST extends AbstractFacade<County> {
    private static final Logger LOGGER = Logger.getLogger(County.class.getName());
    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    public CountyFacadeREST() {
        super(County.class);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    public List<County> findAll() {
        return super.findAll();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
