/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.EventTicketModel;
import com.ticketing.custom.MyConstants;
import com.ticketing.custom.MyResponse;
import com.ticketing.custom.MyStatus;
import com.ticketing.rest.EventTickets;
import com.ticketing.rest.Users;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pc
 */
@Stateless
@Path("eventtickets")
public class EventTicketsFacadeREST extends AbstractFacade<EventTickets> {
    private static final Logger LOGGER = Logger.getLogger(EventTickets.class.getName());
    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    public EventTicketsFacadeREST() {
        super(EventTickets.class);
    }
    @EJB
    UsersFacadeREST usersFacadeREST;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createRecord(EventTicketModel eventTicket) {
        try {
            Users user = usersFacadeREST.find(eventTicket.getCreatedBy().getId());            
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EventTickets> cq = cb.createQuery(EventTickets.class);
            Root<EventTickets> root = cq.from(EventTickets.class);
            cq.where(cb.and(cb.equal(root.get("statusId"), MyConstants.STATUS_ACTIVE),cb.equal(root.get("eventsId"), eventTicket.getEventsId()), cb.equal(root.get("ticketTypeId"), eventTicket.getTicketTypeId()), cb.equal(root.get("ticketNameId"), eventTicket.getTicketNameId()), cb.equal(root.get("merchantId"), user.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            List<EventTickets> eventTickets = q.getResultList();
            if(!eventTickets.isEmpty()){
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_EXIST_STATUS, MyConstants.RESPONSE_EXIST_MESSAGE), "")).build();
            }
            
            Date startTime = myCommon.strToDateTimeConvert(eventTicket.getPurchaseStartDateTime()+ ":00");
            Date endTime = myCommon.strToDateTimeConvert(eventTicket.getPurchaseEndDateTime() + ":00");
            EventTickets entity = new EventTickets();
            entity.setTicketNameId(eventTicket.getTicketNameId());
            entity.setTicketTypeId(eventTicket.getTicketTypeId());
            entity.setTicketQuantity(eventTicket.getTicketQuantity());
            entity.setCreatedBy(eventTicket.getCreatedBy());
            entity.setEventsId(eventTicket.getEventsId());
            entity.setPrice(eventTicket.getPrice());
            entity.setPurchaseEndDateTime(startTime);
            entity.setPurchaseEndDateTime(endTime);
            entity.setValidTill(endTime);
            entity.setCreatedAt(myCommon.myDate());
            entity.setMerchantId(user.getMerchantId());
            entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_ACTIVE)); 
            entity.setRemainingTickets(entity.getTicketQuantity());
            super.create(entity);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Ticket Tiers Creation {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editRecord(EventTickets entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());            
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EventTickets> cq = cb.createQuery(EventTickets.class);
            Root<EventTickets> root = cq.from(EventTickets.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), user.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            EventTickets eventTicket = (EventTickets)q.getSingleResult();
            int ticketDifferent = eventTicket.getRemainingTickets() - entity.getRemainingTickets();
            eventTicket.setRemainingTickets(entity.getRemainingTickets());
            eventTicket.setTicketQuantity(eventTicket.getTicketQuantity()-ticketDifferent);           
            super.edit(eventTicket);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Ticket Tier Updating {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteRecord(EventTickets entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());            
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EventTickets> cq = cb.createQuery(EventTickets.class);
            Root<EventTickets> root = cq.from(EventTickets.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), user.getMerchantId()), cb.equal(root.get("statusId"), MyConstants.STATUS_ACTIVE)));
            Query q = getEntityManager().createQuery(cq);
            EventTickets eventTicket = (EventTickets)q.getSingleResult();
            if(eventTicket.getRemainingTickets() < entity.getTicketQuantity()){
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_STATUS, MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_MSG), "")).build();
            }
            eventTicket.setStatusId(myCommon.getStatus(MyConstants.STATUS_DELETED));            
            super.edit(eventTicket);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Ticket Tier Deleting {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("{id}/{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findOne(@PathParam("id") Integer id, @PathParam("userId") Integer userId) {
        try {
            Users user = usersFacadeREST.find(userId);
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EventTickets> cq = cb.createQuery(EventTickets.class);
            Root<EventTickets> root = cq.from(EventTickets.class);
            cq.where(cb.and(cb.equal(root.get("id"), id), cb.equal(root.get("merchantId"), user.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            return Response.ok((EventTickets) q.getSingleResult()).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("all/{eventsId}/{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<EventTickets> getAll(@PathParam("eventsId") Integer eventsId, @PathParam("userId") Integer userId, @QueryParam("statusId") String statusId) {
        Users user = usersFacadeREST.find(userId);
        return this.getEventTickets(eventsId, user.getMerchantId().getId(),statusId);
    }

    //function to get event tickets
    public List<EventTickets> getEventTickets(int eventsId, int merchantId, String statusId){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<EventTickets> cq = cb.createQuery(EventTickets.class);
        Root<EventTickets> root = cq.from(EventTickets.class);
        if (statusId.isEmpty()) {
            cq.where(cb.and(cb.equal(root.get("eventsId"), eventsId),cb.equal(root.get("merchantId"), merchantId)));
        } else {
            cq.where(cb.and(cb.equal(root.get("eventsId"), eventsId),cb.equal(root.get("statusId"), Integer.parseInt(statusId)), cb.equal(root.get("merchantId"), merchantId)));
        }
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
