/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.MyConstants;
import com.ticketing.custom.MyHash;
import com.ticketing.custom.MyResponse;
import com.ticketing.custom.MyStatus;
import com.ticketing.rest.Token;
import com.ticketing.rest.Users;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pc
 */
@Stateless
@Path("token")
public class TokenFacadeREST extends AbstractFacade<Token> {

    private static final Logger LOGGER = Logger.getLogger(Token.class.getName());
    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    public TokenFacadeREST() {
        super(Token.class);
    }

    @EJB
    UsersFacadeREST usersFacadeREST;
    @EJB
    CommunicationsFacade communicationsFacade;
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response verifyToken(Token entity) {
       try {
           //validate access points using app-key [to be implemented]
            String token = entity.getToken();
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Token> cq = cb.createQuery(Token.class);
            Root<Token> cpRoot = cq.from(Token.class);
            cq.where(cb.and(cb.equal(cpRoot.get("token"), token), cb.equal(cpRoot.get("statusId"), MyConstants.STATUS_ACTIVE)));
            Query q = getEntityManager().createQuery(cq);
            Token data = (Token) q.getSingleResult();
            data.setVerifiedAt(myCommon.myDate());
            data.setStatusId(myCommon.getStatus(MyConstants.STATUS_VERIFIED));
            super.edit(data);

            Users user = usersFacadeREST.find(data.getUsersId().getId());
            usersFacadeREST.edit(user); // update user details
            //log activation communication - email
            communicationsFacade.accountActivationEmail(user);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), data.getUsersId().getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Traverse Inner {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    
    }
    
    public Token createToken(Users entity){
        //generate and save token
        Token token = new Token();
        MyHash myHash = new MyHash();
        token.setStatusId(myCommon.getStatus(MyConstants.STATUS_ACTIVE));        
        token.setUsersId(entity);
        token.setToken(myHash.generateToken());
        token.setPurposeCode(MyConstants.TOKEN_PURPOSE_ACCOUNTS);
        token.setCreatedBy(entity);
        token.setCreatedAt(myCommon.myDate());
        super.create(token);
        return token;
    }

    public Token getToken(Users User) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Token> cq = cb.createQuery(Token.class);
        Root<Token> cpRoot = cq.from(Token.class);
        cq.where(cb.and(cb.equal(cpRoot.get("usersId"), User.getId()),cb.equal(cpRoot.get("statusId"), MyConstants.STATUS_ACTIVE)));
        Query q = getEntityManager().createQuery(cq);
        Token token = new Token();
        List<Token> tokens = q.getResultList();
        for (Token t : tokens) {
            token = t;
        }
        return token;
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
