/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.ChangePassword;
import com.ticketing.custom.MyConstants;
import com.ticketing.custom.MyHash;
import com.ticketing.custom.MyResponse;
import com.ticketing.custom.MyStatus;
import com.ticketing.rest.Users;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pc
 */
@Stateless
@Path("users")
public class UsersFacadeREST extends AbstractFacade<Users> {

    private static final Logger LOGGER = Logger.getLogger(Users.class.getName());
    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    public UsersFacadeREST() {
        super(Users.class);
    }
    @EJB
    TokenFacadeREST tokenFacadeREST;
    @EJB
    CommunicationsFacade communicationsFacade;
    @EJB
    WalletCreationFacade walletCreationFacade;
    @EJB
    MerchantsFacade merchantsFacade;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createAccount(@Context HttpHeaders headers, Users entity) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Users> cq = cb.createQuery(Users.class);
            Root<Users> root = cq.from(Users.class);
            cq.where(cb.equal(root.get("email"), entity.getEmail()));
            Query q = getEntityManager().createQuery(cq);
            q.getSingleResult();
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_EXIST_STATUS, MyConstants.RESPONSE_EXIST_MESSAGE), "")).build();
        } catch (Exception e) {
            try {
                String appKey = myCommon.getHeaderKey(headers, "app-key");
                if (!myCommon.validateAppAccess(entity.getUserTypesId().getId(), appKey)) {
                    return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_ACCESS_DENIED_STATUS, MyConstants.RESPONSE_ACCESS_DENIED_MSG), "")).build();
                }

                //validate the roles of users being created
                if (entity.getUserRolesId().getId().equals(MyConstants.USER_ROLE_SUPER_ADMIN)) {
                    return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_STATUS, MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_MSG), "")).build();
                }

                //validate if user creating the account has the required privilage / roles ::- when creating merchant and administrator users
                if (entity.getUserRolesId().getId().equals(MyConstants.USER_ROLE_ADMINISTRATOR)) {
                    Users creatingUsers = super.find(entity.getCreatedBy());
                    if (!creatingUsers.getUserRolesId().getId().equals(MyConstants.USER_ROLE_SUPER_ADMIN)) {
                        return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_STATUS, MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_MSG), "")).build();
                    }
                }

                if (entity.getUserRolesId().getId().equals(MyConstants.USER_ROLE_MERCHANT_USER)) {
                    Users creatingUsers = super.find(entity.getCreatedBy());
                    if (!creatingUsers.getUserRolesId().getId().equals(MyConstants.USER_ROLE_MERCHANT_SUPER_ADMIN)) {
                        return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_STATUS, MyConstants.RESPONSE_PROCESS_NOT_ALLOWED_MSG), "")).build();
                    }
                }

                this.createdAccount(entity);
                tokenFacadeREST.createToken(entity);//token
                communicationsFacade.accountCreationEmail(entity);//email  logging

                if (entity.getUserRolesId().getId().equals(MyConstants.USER_ROLE_CLIENT)) {
                    //create / check wallet details
                    walletCreationFacade.logWalletCreation(entity);
                }
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();

            } catch (Exception ex) {
                LOGGER.log(Level.INFO, "Account Creation {0}", e);
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
            }
        }
    }

    @POST
    @Path("/login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response login(@Context HttpHeaders headers, Users entity) {
        try {
            String appKey = myCommon.getHeaderKey(headers, "app-key");
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Users> cq = cb.createQuery(Users.class);
            Root<Users> root = cq.from(Users.class);
            cq.where(cb.equal(root.get("email"), entity.getEmail()));
            Query q = getEntityManager().createQuery(cq);
            Users user = (Users) q.getSingleResult();

            //validate app key
            if (!myCommon.validateAppAccess(user.getUserTypesId().getId(), appKey)) {
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_ACCESS_DENIED_STATUS, MyConstants.RESPONSE_ACCESS_DENIED_MSG), "")).build();
            }

            //validate account status
            if (!user.getStatusId().getId().equals(MyConstants.STATUS_ACTIVE)) {
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_ACCESS_DENIED_STATUS, MyConstants.RESPONSE_ACCESS_DENIED_MSG), "")).build();
            }

            MyHash myHash = new MyHash();
            if (myHash.compareHash(entity.getPassword(), user.getPassword())) {
                user.setLastLogin(myCommon.myDate());
                user.setLoginAttempts(0);
                super.edit(user);//update last login
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), user.getId().toString())).build();
            } else {
                int loginAttepts = user.getLoginAttempts() + 1;
                user.setLoginAttempts(loginAttepts);
                if (loginAttepts == 5) {
                    user.setStatusId(myCommon.getStatus(MyConstants.STATUS_BLOCKED));
                }
                super.edit(user);
            }
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_PASSWORD_DOES_NOT_MATCH_STATUS, MyConstants.RESPONSE_PASSWORD_USERNAME_DOES_NOT_MATCH_MSG), "")).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "User Login {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @POST
    @Path("resetpassword")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response resetPassword(@Context HttpHeaders headers, Users entity) {
        try {
            String appKey = myCommon.getHeaderKey(headers, "app-key");
            Users user = super.find(entity.getId());

            //validate app key
            if (!myCommon.validateAppAccess(user.getUserTypesId().getId(), appKey)) {
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_ACCESS_DENIED_STATUS, MyConstants.RESPONSE_ACCESS_DENIED_MSG), "")).build();
            }
            MyHash hash = new MyHash();
            user.setPassword(hash.hashData(entity.getPassword()));
            user.setLoginAttempts(0);
            user.setStatusId(myCommon.getStatus(MyConstants.STATUS_ACTIVE));
            user.setVerifiedAt(myCommon.myDate());
            super.edit(user);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @POST
    @Path("changepassword")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response changePassword(@Context HttpHeaders headers, ChangePassword entity) {
        try {
            String appKey = myCommon.getHeaderKey(headers, "app-key");
            Users user = super.find(entity.getId());

            //validate app key
            if (!myCommon.validateAppAccess(user.getUserTypesId().getId(), appKey)) {
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_ACCESS_DENIED_STATUS, MyConstants.RESPONSE_ACCESS_DENIED_MSG), "")).build();
            }
            MyHash hash = new MyHash();
            if (hash.compareHash(entity.getCurrentPassword(), user.getPassword())) {
                user.setPassword(hash.hashData(entity.getNewPassword()));
                super.edit(user);
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), user.getId().toString())).build();
            }
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_PASSWORD_DOES_NOT_MATCH_STATUS, MyConstants.RESPONSE_PASSWORD_DOES_NOT_MATCH_MSG), String.valueOf(entity.getId()))).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @POST
    @Path("requestpasswordreset")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response requestPasswordReset(@Context HttpHeaders headers, Users entity) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Users> cq = cb.createQuery(Users.class);
            Root<Users> root = cq.from(Users.class);
            cq.where(cb.and(cb.equal(root.get("email"), entity.getEmail()), cb.notEqual(root.get("statusId"), MyConstants.STATUS_DELETED)));
            Query q = getEntityManager().createQuery(cq);
            Users user = (Users) q.getSingleResult();
            String appKey = myCommon.getHeaderKey(headers, "app-key");
            //validate app key
            if (!myCommon.validateAppAccess(user.getUserTypesId().getId(), appKey)) {
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_ACCESS_DENIED_STATUS, MyConstants.RESPONSE_ACCESS_DENIED_MSG), "")).build();
            }
            tokenFacadeREST.createToken(user);
            communicationsFacade.accountPasswordResetRequestEmail(user);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), user.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Request Password Reset {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_USERNAME_DOES_NOT_EXIST_MSG), "")).build();
        }
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteUser(Users entity) {
        try {
            //check user roles
            Users clientUser = super.find(entity.getId());
            clientUser.setStatusId(myCommon.getStatus(MyConstants.STATUS_DELETED));
            super.edit(clientUser);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getOne(@PathParam("id") Integer id) {
        try {
            return Response.ok(super.find(id)).build();
        } catch (NoResultException nre) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<Users> getAll(@Context HttpHeaders headers, @QueryParam("merchantId") String merchantId, @QueryParam("statusId") String statusId) {
        String appKey = myCommon.getHeaderKey(headers, "app-key");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> cq = cb.createQuery(Users.class);
        Root<Users> root = cq.from(Users.class);
        if (appKey.equals(MyConstants.APP_KEY_MERCHANT)) {
            if (!statusId.equals("")) {
                cq.where(cb.and(cb.equal(root.get("merchantId"), Integer.parseInt(merchantId)), cb.equal(root.get("statusId"), Integer.parseInt(statusId)), cb.notEqual(root.get("userRolesId"), MyConstants.USER_ROLE_MERCHANT_SUPER_ADMIN)));
            } else {
                cq.where(cb.and(cb.equal(root.get("merchantId"), Integer.parseInt(merchantId)), cb.notEqual(root.get("userRolesId"), MyConstants.USER_ROLE_MERCHANT_SUPER_ADMIN)));
            }
        }else{
            if (!statusId.equals("")) {
                  cq.where(cb.and(cb.equal(root.get("userTypesId"), MyConstants.USER_ACCOUNT_TYPE_ADMINISTRATOR), cb.equal(root.get("statusId"), Integer.parseInt(statusId)), cb.notEqual(root.get("userRolesId"), MyConstants.USER_ROLE_SUPER_ADMIN)));
            }else{
                 cq.where(cb.and(cb.equal(root.get("userTypesId"), MyConstants.USER_ACCOUNT_TYPE_ADMINISTRATOR), cb.notEqual(root.get("userRolesId"), MyConstants.USER_ROLE_SUPER_ADMIN)));         
            }          
        }

        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Users createdAccount(Users entity) {
        MyHash myHash = new MyHash();
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setCreatedAt(myCommon.myDate());
        entity.setPassword(myHash.hashData(entity.getPhoneNumber()));
        super.create(entity);
        return entity;
    }

    public Users getMerchantId(int userId) {
        Users entity = super.find(userId);
        if (entity.getUserRolesId().getId().equals(MyConstants.USER_ROLE_MERCHANT_USER)) {
            entity = super.find(entity.getCreatedBy());
        }
        return entity;
    }

    public Users getUserByEmail(String email) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Users> cq = cb.createQuery(Users.class);
        Root<Users> root = cq.from(Users.class);
        cq.where(cb.equal(root.get("email"), email));
        Query q = getEntityManager().createQuery(cq);
        return (Users) q.getSingleResult();
    }
}
