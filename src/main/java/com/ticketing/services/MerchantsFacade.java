/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.MyConstants;
import com.ticketing.custom.MyResponse;
import com.ticketing.custom.MyStatus;
import com.ticketing.rest.Merchants;
import com.ticketing.rest.Users;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pc
 */
@Stateless
@Path("merchants")
public class MerchantsFacade extends AbstractFacade<Merchants> {
    
    private static final Logger LOGGER = Logger.getLogger(Merchants.class.getName());
    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public MerchantsFacade() {
        super(Merchants.class);
    }
    
    @EJB
    UsersFacadeREST usersFacadeREST;
    @EJB
    CommunicationsFacade communicationsFacade;
    @EJB
    TokenFacadeREST tokenFacadeREST;
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createMerchant(Merchants entity) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Merchants> cq = cb.createQuery(Merchants.class);
            Root<Merchants> root = cq.from(Merchants.class);
            cq.where(cb.or(cb.equal(root.get("email"), entity.getEmail()), cb.equal(root.get("contactPersonEmail"), entity.getContactPersonEmail()), cb.equal(root.get("businessName"), entity.getBusinessName())));
            Query q = getEntityManager().createQuery(cq);
            q.getSingleResult();
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_EXIST_STATUS, MyConstants.RESPONSE_EXIST_MESSAGE), "")).build();
        } catch (Exception e) {
            try {
                entity.setCreatedAt(myCommon.myDate());
                entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
                super.create(entity);
                
                Users user = new Users();
                user.setEmail(entity.getContactPersonEmail());
                user.setMerchantId(entity);
                user.setName(entity.getContactPersonFirstName() + " " + entity.getContactPersonLastName());
                user.setPhoneNumber(entity.getContactPersonPhoneNumber());
                user.setUserRolesId(myCommon.getRole(MyConstants.USER_ROLE_MERCHANT_SUPER_ADMIN));
                user.setUserTypesId(myCommon.getUserTypes(MyConstants.USER_ACCOUNT_TYPE_MERCHANT));
                user = usersFacadeREST.createdAccount(user);

                //log communication
                communicationsFacade.merchantCreationEmail(entity); //merchant creation email
                tokenFacadeREST.createToken(user);//token
                communicationsFacade.accountCreationEmail(user);//email  logging
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
            } catch (Exception ex) {
                LOGGER.log(Level.INFO, "Merchant Creation {0}", ex);
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
            }
        }
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editRecord(Merchants entity) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Merchants> cq = cb.createQuery(Merchants.class);
            Root<Merchants> root = cq.from(Merchants.class);
            cq.where(cb.and(cb.notEqual(root.get("id"), entity.getId()), cb.or(cb.equal(root.get("email"), entity.getEmail()), cb.equal(root.get("contactPersonEmail"), entity.getContactPersonEmail()), cb.equal(root.get("businessName"), entity.getBusinessName()))));
            Query q = getEntityManager().createQuery(cq);
            q.getSingleResult();
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_EXIST_STATUS, MyConstants.RESPONSE_EXIST_MESSAGE), "")).build();
        } catch (Exception e) {
            try {
                Merchants merchant = super.find(entity.getId());
                String email = merchant.getEmail();
                entity.setVerifiedBy(merchant.getVerifiedBy());
                entity.setVerifiedAt(merchant.getVerifiedAt());
                entity.setStatusId(merchant.getStatusId());
                entity.setCreatedAt(merchant.getCreatedAt());
                entity.setRemarks(merchant.getRemarks());
                super.edit(entity);
                
                Users user = usersFacadeREST.getUserByEmail(email);
                user.setEmail(entity.getContactPersonEmail());
                user.setName(entity.getContactPersonFirstName() + " " + entity.getContactPersonLastName());
                user.setPhoneNumber(entity.getContactPersonPhoneNumber());

                //log updating communication
                communicationsFacade.merchantUpdatingEmail(entity);
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), merchant.getId().toString())).build();
                
            } catch (Exception ex) {
                LOGGER.log(Level.INFO, "Merchant Verification {0}", e);
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
            }
        }
    }
    
    @PUT
    @Path("verify")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response verifyAccount(Merchants entity, @QueryParam("action") String action) {
        try {
            Merchants merchant = super.find(entity.getId());
            //run the verification function
            merchant.setVerifiedBy(entity.getVerifiedBy());
            merchant.setRemarks(entity.getRemarks());
            merchant.setVerifiedAt(myCommon.myDate());
            if (action.equals("1")) {
                merchant.setStatusId(myCommon.getStatus(MyConstants.STATUS_ACTIVE));
            } else {
                merchant.setStatusId(myCommon.getStatus(MyConstants.STATUS_REJECTED));
            }
            super.edit(merchant);
            //log verification communication
            communicationsFacade.merchantVerificationEmail(entity);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), merchant.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Merchant Verification {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }
    
    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteRecord(Merchants entity) {
        try {
            Merchants merchant = super.find(entity.getId());
            merchant.setRemarks(entity.getRemarks());
            merchant.setStatusId(myCommon.getStatus(MyConstants.STATUS_DELETED));
            super.edit(merchant);

            //log merchant creation communication
            communicationsFacade.merchantDeletingEmail(entity);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), merchant.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Merchant Deletion {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Merchants> getAll(@QueryParam("statusId") String statusId) {
        return this.getRecords(statusId);
    }
    
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Merchants getOne(@PathParam("id") int id) {
        return super.find(id);
    }
    //get all record

    public List<Merchants> getRecords(String statusId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Merchants> cq = cb.createQuery(Merchants.class);
        Root<Merchants> root = cq.from(Merchants.class);
        if (!statusId.isEmpty()) {
            cq.where(cb.equal(root.get("statusId"), Integer.parseInt(statusId)));
        }
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
}
