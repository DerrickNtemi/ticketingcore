/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.MyConstants;
import com.ticketing.rest.Communication;
import com.ticketing.rest.Merchants;
import com.ticketing.rest.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Path;

/**
 *
 * @author pc
 */
@Stateless
@Path("communications")
public class CommunicationsFacade extends AbstractFacade<Communication> {

    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommunicationsFacade() {
        super(Communication.class);
    }
    
    
    public void accountCreationEmail(Users user){
        Communication entity = new Communication();
        entity.setCommunicationTypeCode(MyConstants.COMMUNICATION_TYPE_EMAIL);
        entity.setCreatedAt(myCommon.myDate());
        entity.setMessage("Account Creation");
        entity.setReceiver(user.getId().toString());
        entity.setSender(MyConstants.COMMUNICATION_SENDER_ADMINISTRATOR);
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setSubject(MyConstants.COMMUNICATION_TYPE_ACCOUNT_CREATION);
        super.create(entity);
    }

    //password reset email logging
    public void accountPasswordResetRequestEmail(Users user) {
        Communication entity = new Communication();
        entity.setCommunicationTypeCode(MyConstants.COMMUNICATION_TYPE_EMAIL);
        entity.setCreatedAt(myCommon.myDate());
        entity.setMessage("Account Password Reset Request");
        entity.setReceiver(user.getId().toString());
        entity.setSender(MyConstants.COMMUNICATION_SENDER_ADMINISTRATOR);
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setSubject(MyConstants.COMMUNICATION_TYPE_PASSWORD_RESET_REQUEST);
        super.create(entity);
    }

    //account activation email logging
    public void accountActivationEmail(Users user) {
        Communication entity = new Communication();
        entity.setCommunicationTypeCode(MyConstants.COMMUNICATION_TYPE_EMAIL);
        entity.setCreatedAt(myCommon.myDate());
        entity.setMessage("Account Activation Reset");
        entity.setReceiver(user.getId().toString());
        entity.setSender(MyConstants.COMMUNICATION_SENDER_ADMINISTRATOR);
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setSubject(MyConstants.COMMUNICATION_TYPE_ACCOUNT_ACTIVATION);
        super.create(entity);
    }

    //merchant verification email logging
    public void merchantCreationEmail(Merchants merchant) {
        Communication entity = new Communication();
        entity.setCommunicationTypeCode(MyConstants.COMMUNICATION_TYPE_EMAIL);
        entity.setCreatedAt(myCommon.myDate());
        entity.setMessage("Merchant Creation");
        entity.setReceiver(merchant.getId().toString());
        entity.setSender(MyConstants.COMMUNICATION_SENDER_ADMINISTRATOR);
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setSubject(MyConstants.COMMUNICATION_TYPE_MERCHANT_CREATION);
        super.create(entity);
    }

    //merchant verification email logging
    public void merchantVerificationEmail(Merchants merchant) {
        Communication entity = new Communication();
        entity.setCommunicationTypeCode(MyConstants.COMMUNICATION_TYPE_EMAIL);
        entity.setCreatedAt(myCommon.myDate());
        entity.setMessage("Merchant Verification");
        entity.setReceiver(merchant.getId().toString());
        entity.setSender(MyConstants.COMMUNICATION_SENDER_ADMINISTRATOR);
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setSubject(MyConstants.COMMUNICATION_TYPE_MERCHANT_VERIFICATION);
        super.create(entity);
    }

    //merchant profile update information logging
    public void merchantUpdatingEmail(Merchants merchants) {
        Communication entity = new Communication();
        entity.setCommunicationTypeCode(MyConstants.COMMUNICATION_TYPE_EMAIL);
        entity.setCreatedAt(myCommon.myDate());
        entity.setMessage("Merchant Updating");
        entity.setReceiver(merchants.getId().toString());
        entity.setSender(MyConstants.COMMUNICATION_SENDER_ADMINISTRATOR);
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setSubject(MyConstants.COMMUNICATION_TYPE_MERCHANT_UPDATING);
        super.create(entity);
    }

    //merchant verification email logging
    public void merchantDeletingEmail(Merchants merchant) {
        Communication entity = new Communication();
        entity.setCommunicationTypeCode(MyConstants.COMMUNICATION_TYPE_EMAIL);
        entity.setCreatedAt(myCommon.myDate());
        entity.setMessage("Merchant Deleting");
        entity.setReceiver(merchant.getId().toString());
        entity.setSender(MyConstants.COMMUNICATION_SENDER_ADMINISTRATOR);
        entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_PENDING));
        entity.setSubject(MyConstants.COMMUNICATION_TYPE_MERCHANT_DELETING);
        super.create(entity);
    }
    
    //get pending communications - [queued for send]
    public List<Communication> findByStatus() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Communication> cq = cb.createQuery(Communication.class);
        Root<Communication> cpRoot = cq.from(Communication.class);
        cq.where(cb.equal(cpRoot.get("statusId"), MyConstants.STATUS_PENDING));
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
}
