/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author pc
 */
@javax.ws.rs.ApplicationPath("services")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.ticketing.services.CommunicationsFacade.class);
        resources.add(com.ticketing.services.CountryFacadeREST.class);
        resources.add(com.ticketing.services.CountyFacadeREST.class);
        resources.add(com.ticketing.services.EventGalleryFacade.class);
        resources.add(com.ticketing.services.EventSponsorFacade.class);
        resources.add(com.ticketing.services.EventTicketsFacadeREST.class);
        resources.add(com.ticketing.services.EventTypeFacadeREST.class);
        resources.add(com.ticketing.services.EventsFacadeREST.class);
        resources.add(com.ticketing.services.MerchantsFacade.class);
        resources.add(com.ticketing.services.StatusFacadeREST.class);
        resources.add(com.ticketing.services.TicketNameFacadeREST.class);
        resources.add(com.ticketing.services.TicketPurchasesFacadeREST.class);
        resources.add(com.ticketing.services.TicketTypeFacadeREST.class);
        resources.add(com.ticketing.services.TokenFacadeREST.class);
        resources.add(com.ticketing.services.UserRoleFacadeREST.class);
        resources.add(com.ticketing.services.UserTypeFacadeREST.class);
        resources.add(com.ticketing.services.UsersFacadeREST.class);
        resources.add(com.ticketing.services.WalletCreationFacade.class);
    }
    
}
