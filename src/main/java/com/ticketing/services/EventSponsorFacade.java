/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.MyConstants;
import com.ticketing.custom.MyResponse;
import com.ticketing.custom.MyStatus;
import com.ticketing.rest.EventSponsor;
import com.ticketing.rest.EventTickets;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pc
 */
@Stateless
@Path("eventsponsor")
public class EventSponsorFacade extends AbstractFacade<EventSponsor> {
    private static final Logger LOGGER = Logger.getLogger(EventSponsor.class.getName());

    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EventSponsorFacade() {
        super(EventSponsor.class);
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createRecord(EventSponsor entity) {
        try {           
            entity.setCreatedAt(myCommon.myDate());
            entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_ACTIVE));
            super.create(entity);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Sponsors {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }
    
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editRecord(EventSponsor entity) {
        try {           
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EventSponsor> cq = cb.createQuery(EventSponsor.class);
            Root<EventSponsor> root = cq.from(EventSponsor.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), entity.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            EventSponsor eventSponsor = (EventSponsor)q.getSingleResult();
            entity.setCreatedAt(eventSponsor.getCreatedAt());
            entity.setEventId(eventSponsor.getEventId());  
            entity.setMerchantId(eventSponsor.getMerchantId());
            entity.setStatusId(eventSponsor.getStatusId());
            super.edit(entity);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Sponsor {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteRecord(EventSponsor entity) {
        try {            
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EventSponsor> cq = cb.createQuery(EventSponsor.class);
            Root<EventSponsor> root = cq.from(EventSponsor.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), entity.getMerchantId()), cb.equal(root.get("statusId"), MyConstants.STATUS_ACTIVE)));
            Query q = getEntityManager().createQuery(cq);
            EventSponsor eventSponsor = (EventSponsor)q.getSingleResult();
            eventSponsor.setStatusId(myCommon.getStatus(MyConstants.STATUS_DELETED));            
            super.edit(eventSponsor);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Events Ticket Tier Deleting {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("{id}/{merchantId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findOne(@PathParam("id") int id, @PathParam("merchantId") int merchantId) {
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<EventSponsor> cq = cb.createQuery(EventSponsor.class);
            Root<EventSponsor> root = cq.from(EventSponsor.class);
            cq.where(cb.and(cb.equal(root.get("id"), id), cb.equal(root.get("merchantId"), merchantId)));
            Query q = getEntityManager().createQuery(cq);
            return Response.ok((EventSponsor) q.getSingleResult()).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("all/{eventId}/{merchantId}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<EventSponsor> getAll(@PathParam("eventId") int eventId, @PathParam("merchantId") int merchantId, @QueryParam("statusId") String statusId) {
        return this.getEventSponsors(eventId, merchantId, statusId);
    }
    
    //function to get event sponsors
    public List<EventSponsor> getEventSponsors(int eventId, int merchantId, String statusId){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<EventSponsor> cq = cb.createQuery(EventSponsor.class);
        Root<EventSponsor> root = cq.from(EventSponsor.class);
        if (statusId.isEmpty()) {
            cq.where(cb.and(cb.equal(root.get("eventId"), eventId),cb.equal(root.get("merchantId"), merchantId)));
        } else {
            cq.where(cb.and(cb.equal(root.get("eventId"), eventId),cb.equal(root.get("statusId"), Integer.parseInt(statusId)), cb.equal(root.get("merchantId"), merchantId)));
        }
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
}
