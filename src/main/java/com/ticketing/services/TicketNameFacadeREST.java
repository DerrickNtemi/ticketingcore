/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.services;

import com.ticketing.custom.MyConstants;
import com.ticketing.custom.MyResponse;
import com.ticketing.custom.MyStatus;
import com.ticketing.rest.TicketName;
import com.ticketing.rest.Users;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author pc
 */
@Stateless
@Path("ticketname")
public class TicketNameFacadeREST extends AbstractFacade<TicketName> {

    private static final Logger LOGGER = Logger.getLogger(TicketName.class.getName());
    @PersistenceContext(unitName = "com.ssd_ticketingcore_war_1.0PU")
    private EntityManager em;

    public TicketNameFacadeREST() {
        super(TicketName.class);
    }
    @EJB
    UsersFacadeREST usersFacadeREST;

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response createRecord(TicketName entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<TicketName> cq = cb.createQuery(TicketName.class);
            Root<TicketName> root = cq.from(TicketName.class);
            cq.where(cb.and(cb.equal(root.get("name"), entity.getName()), cb.equal(root.get("merchantId"), user.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            List<TicketName> ticketNames = q.getResultList();
            if (!ticketNames.isEmpty()) {
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_EXIST_STATUS, MyConstants.RESPONSE_EXIST_MESSAGE), "")).build();
            }
            entity.setCreatedAt(myCommon.myDate());
            entity.setStatusId(myCommon.getStatus(MyConstants.STATUS_ACTIVE));
            entity.setMerchantId(user.getMerchantId());
            super.create(entity);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response editRecord(TicketName entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<TicketName> cq = cb.createQuery(TicketName.class);
            Root<TicketName> root = cq.from(TicketName.class);
            cq.where(cb.and(cb.notEqual(root.get("id"), entity.getId()), cb.equal(root.get("name"), entity.getName()), cb.equal(root.get("merchantId"), user.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            List<TicketName> ticketNames = q.getResultList();
            if (!ticketNames.isEmpty()) {
                return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_EXIST_STATUS, MyConstants.RESPONSE_EXIST_MESSAGE), "")).build();
            }
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), user.getMerchantId())));
            q = getEntityManager().createQuery(cq);
            TicketName ticketName = (TicketName) q.getSingleResult();
            ticketName.setName(entity.getName());
            super.create(ticketName);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            LOGGER.log(Level.INFO, "Ticket Name Updating {0}", e);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteRecord(TicketName entity) {
        try {
            Users user = usersFacadeREST.find(entity.getCreatedBy().getId());
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<TicketName> cq = cb.createQuery(TicketName.class);
            Root<TicketName> root = cq.from(TicketName.class);
            cq.where(cb.and(cb.equal(root.get("id"), entity.getId()), cb.equal(root.get("merchantId"), user.getMerchantId()), cb.equal(root.get("statusId"), MyConstants.STATUS_ACTIVE)));
            Query q = getEntityManager().createQuery(cq);
            TicketName ticketName = (TicketName) q.getSingleResult();
            ticketName.setStatusId(myCommon.getStatus(MyConstants.STATUS_DELETED));
            super.create(ticketName);
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_OK_STATUS, MyConstants.RESPONSE_OK_MESSAGE), entity.getId().toString())).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_FAILED_STATUS, MyConstants.RESPONSE_FAILED_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("{id}/{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response find(@PathParam("id") Integer id, @PathParam("userId") Integer userId) {
        try {
            Users user = usersFacadeREST.find(userId);
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<TicketName> cq = cb.createQuery(TicketName.class);
            Root<TicketName> root = cq.from(TicketName.class);
            cq.where(cb.and(cb.equal(root.get("id"), id), cb.equal(root.get("merchantId"), user.getMerchantId())));
            Query q = getEntityManager().createQuery(cq);
            TicketName ticketName = (TicketName) q.getSingleResult();
            return Response.ok(ticketName).build();
        } catch (Exception e) {
            return Response.ok(new MyResponse(new MyStatus(MyConstants.RESPONSE_NOT_FOUND_STATUS, MyConstants.RESPONSE_NOT_FOUND_MESSAGE), "")).build();
        }
    }

    @GET
    @Path("{userId}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<TicketName> getAll(@PathParam("userId") Integer userId, @QueryParam("statusId") String statusId) {

        Users user = usersFacadeREST.find(userId);
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<TicketName> cq = cb.createQuery(TicketName.class);
        Root<TicketName> root = cq.from(TicketName.class);
        if (statusId.isEmpty()) {
            cq.where(cb.equal(root.get("merchantId"), user.getMerchantId()));
        } else {
            cq.where(cb.and(cb.equal(root.get("statusId"), Integer.parseInt(statusId)), cb.equal(root.get("merchantId"), user.getMerchantId())));
        }
        Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
