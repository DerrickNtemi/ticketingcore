/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "eventSponsor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventSponsor.findAll", query = "SELECT e FROM EventSponsor e")
    , @NamedQuery(name = "EventSponsor.findById", query = "SELECT e FROM EventSponsor e WHERE e.id = :id")
    , @NamedQuery(name = "EventSponsor.findByName", query = "SELECT e FROM EventSponsor e WHERE e.name = :name")
    , @NamedQuery(name = "EventSponsor.findByItemSponsoring", query = "SELECT e FROM EventSponsor e WHERE e.itemSponsoring = :itemSponsoring")
    , @NamedQuery(name = "EventSponsor.findByLogo", query = "SELECT e FROM EventSponsor e WHERE e.logo = :logo")
    , @NamedQuery(name = "EventSponsor.findByBusinessUrl", query = "SELECT e FROM EventSponsor e WHERE e.businessUrl = :businessUrl")
    , @NamedQuery(name = "EventSponsor.findByCreatedAt", query = "SELECT e FROM EventSponsor e WHERE e.createdAt = :createdAt")})
public class EventSponsor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 100)
    @Column(name = "itemSponsoring")
    private String itemSponsoring;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "logo")
    private String logo;
    @Size(max = 100)
    @Column(name = "businessUrl")
    private String businessUrl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @JoinColumn(name = "eventId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Events eventId;
    @JoinColumn(name = "merchantId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Merchants merchantId;
    @JoinColumn(name = "statusId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;

    public EventSponsor() {
    }

    public EventSponsor(Integer id) {
        this.id = id;
    }

    public EventSponsor(Integer id, String name, String logo, Date createdAt) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemSponsoring() {
        return itemSponsoring;
    }

    public void setItemSponsoring(String itemSponsoring) {
        this.itemSponsoring = itemSponsoring;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getBusinessUrl() {
        return businessUrl;
    }

    public void setBusinessUrl(String businessUrl) {
        this.businessUrl = businessUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Events getEventId() {
        return eventId;
    }

    public void setEventId(Events eventId) {
        this.eventId = eventId;
    }

    public Merchants getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchants merchantId) {
        this.merchantId = merchantId;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventSponsor)) {
            return false;
        }
        EventSponsor other = (EventSponsor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.EventSponsor[ id=" + id + " ]";
    }
    
}
