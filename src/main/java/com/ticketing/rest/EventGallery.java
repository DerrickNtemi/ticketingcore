/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "eventGallery")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventGallery.findAll", query = "SELECT e FROM EventGallery e")
    , @NamedQuery(name = "EventGallery.findById", query = "SELECT e FROM EventGallery e WHERE e.id = :id")
    , @NamedQuery(name = "EventGallery.findByPhoto", query = "SELECT e FROM EventGallery e WHERE e.photo = :photo")
    , @NamedQuery(name = "EventGallery.findByTitle", query = "SELECT e FROM EventGallery e WHERE e.title = :title")
    , @NamedQuery(name = "EventGallery.findByCreatedAt", query = "SELECT e FROM EventGallery e WHERE e.createdAt = :createdAt")})
public class EventGallery implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "photo")
    private String photo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @JoinColumn(name = "eventId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Events eventId;
    @JoinColumn(name = "merchantId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Merchants merchantId;
    @JoinColumn(name = "statusId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;

    public EventGallery() {
    }

    public EventGallery(Integer id) {
        this.id = id;
    }

    public EventGallery(Integer id, String photo, String title, String description, Date createdAt) {
        this.id = id;
        this.photo = photo;
        this.title = title;
        this.description = description;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Events getEventId() {
        return eventId;
    }

    public void setEventId(Events eventId) {
        this.eventId = eventId;
    }

    public Merchants getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchants merchantId) {
        this.merchantId = merchantId;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventGallery)) {
            return false;
        }
        EventGallery other = (EventGallery) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.EventGallery[ id=" + id + " ]";
    }
    
}
