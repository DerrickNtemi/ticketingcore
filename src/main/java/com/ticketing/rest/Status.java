/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Status.findAll", query = "SELECT s FROM Status s")
    , @NamedQuery(name = "Status.findById", query = "SELECT s FROM Status s WHERE s.id = :id")
    , @NamedQuery(name = "Status.findByName", query = "SELECT s FROM Status s WHERE s.name = :name")})
public class Status implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<EventTickets> eventTicketsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<Users> usersCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<SubEvents> subEventsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<Token> tokenCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<EventGallery> eventGalleryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<WalletCreation> walletCreationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<Communication> communicationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<Merchants> merchantsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<EventSponsor> eventSponsorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<Events> eventsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "statusId")
    private Collection<TicketName> ticketNameCollection;

    public Status() {
    }

    public Status(Integer id) {
        this.id = id;
    }

    public Status(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<EventTickets> getEventTicketsCollection() {
        return eventTicketsCollection;
    }

    public void setEventTicketsCollection(Collection<EventTickets> eventTicketsCollection) {
        this.eventTicketsCollection = eventTicketsCollection;
    }

    @XmlTransient
    public Collection<Users> getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Collection<Users> usersCollection) {
        this.usersCollection = usersCollection;
    }

    @XmlTransient
    public Collection<SubEvents> getSubEventsCollection() {
        return subEventsCollection;
    }

    public void setSubEventsCollection(Collection<SubEvents> subEventsCollection) {
        this.subEventsCollection = subEventsCollection;
    }

    @XmlTransient
    public Collection<Token> getTokenCollection() {
        return tokenCollection;
    }

    public void setTokenCollection(Collection<Token> tokenCollection) {
        this.tokenCollection = tokenCollection;
    }

    @XmlTransient
    public Collection<EventGallery> getEventGalleryCollection() {
        return eventGalleryCollection;
    }

    public void setEventGalleryCollection(Collection<EventGallery> eventGalleryCollection) {
        this.eventGalleryCollection = eventGalleryCollection;
    }

    @XmlTransient
    public Collection<WalletCreation> getWalletCreationCollection() {
        return walletCreationCollection;
    }

    public void setWalletCreationCollection(Collection<WalletCreation> walletCreationCollection) {
        this.walletCreationCollection = walletCreationCollection;
    }

    @XmlTransient
    public Collection<Communication> getCommunicationCollection() {
        return communicationCollection;
    }

    public void setCommunicationCollection(Collection<Communication> communicationCollection) {
        this.communicationCollection = communicationCollection;
    }

    @XmlTransient
    public Collection<Merchants> getMerchantsCollection() {
        return merchantsCollection;
    }

    public void setMerchantsCollection(Collection<Merchants> merchantsCollection) {
        this.merchantsCollection = merchantsCollection;
    }

    @XmlTransient
    public Collection<EventSponsor> getEventSponsorCollection() {
        return eventSponsorCollection;
    }

    public void setEventSponsorCollection(Collection<EventSponsor> eventSponsorCollection) {
        this.eventSponsorCollection = eventSponsorCollection;
    }

    @XmlTransient
    public Collection<Events> getEventsCollection() {
        return eventsCollection;
    }

    public void setEventsCollection(Collection<Events> eventsCollection) {
        this.eventsCollection = eventsCollection;
    }

    @XmlTransient
    public Collection<TicketName> getTicketNameCollection() {
        return ticketNameCollection;
    }

    public void setTicketNameCollection(Collection<TicketName> ticketNameCollection) {
        this.ticketNameCollection = ticketNameCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Status)) {
            return false;
        }
        Status other = (Status) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.Status[ id=" + id + " ]";
    }
    
}
