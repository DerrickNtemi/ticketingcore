/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "ticketPurchases")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TicketPurchases.findAll", query = "SELECT t FROM TicketPurchases t")
    , @NamedQuery(name = "TicketPurchases.findById", query = "SELECT t FROM TicketPurchases t WHERE t.id = :id")
    , @NamedQuery(name = "TicketPurchases.findByPurchaseNumber", query = "SELECT t FROM TicketPurchases t WHERE t.purchaseNumber = :purchaseNumber")
    , @NamedQuery(name = "TicketPurchases.findByEventsId", query = "SELECT t FROM TicketPurchases t WHERE t.eventsId = :eventsId")
    , @NamedQuery(name = "TicketPurchases.findByEventTicketsId", query = "SELECT t FROM TicketPurchases t WHERE t.eventTicketsId = :eventTicketsId")
    , @NamedQuery(name = "TicketPurchases.findByConsumerId", query = "SELECT t FROM TicketPurchases t WHERE t.consumerId = :consumerId")
    , @NamedQuery(name = "TicketPurchases.findByNumberOfTickets", query = "SELECT t FROM TicketPurchases t WHERE t.numberOfTickets = :numberOfTickets")
    , @NamedQuery(name = "TicketPurchases.findByTicketAmount", query = "SELECT t FROM TicketPurchases t WHERE t.ticketAmount = :ticketAmount")
    , @NamedQuery(name = "TicketPurchases.findByTotalAmount", query = "SELECT t FROM TicketPurchases t WHERE t.totalAmount = :totalAmount")
    , @NamedQuery(name = "TicketPurchases.findByAmountPaid", query = "SELECT t FROM TicketPurchases t WHERE t.amountPaid = :amountPaid")
    , @NamedQuery(name = "TicketPurchases.findByBalance", query = "SELECT t FROM TicketPurchases t WHERE t.balance = :balance")
    , @NamedQuery(name = "TicketPurchases.findByCreatedBy", query = "SELECT t FROM TicketPurchases t WHERE t.createdBy = :createdBy")
    , @NamedQuery(name = "TicketPurchases.findByCreatedAt", query = "SELECT t FROM TicketPurchases t WHERE t.createdAt = :createdAt")
    , @NamedQuery(name = "TicketPurchases.findByStatusId", query = "SELECT t FROM TicketPurchases t WHERE t.statusId = :statusId")
    , @NamedQuery(name = "TicketPurchases.findByVerifiiedAt", query = "SELECT t FROM TicketPurchases t WHERE t.verifiiedAt = :verifiiedAt")
    , @NamedQuery(name = "TicketPurchases.findByVerifiedBy", query = "SELECT t FROM TicketPurchases t WHERE t.verifiedBy = :verifiedBy")})
public class TicketPurchases implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "purchaseNumber")
    private int purchaseNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "eventsId")
    private int eventsId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "eventTicketsId")
    private int eventTicketsId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "consumerId")
    private int consumerId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numberOfTickets")
    private int numberOfTickets;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ticketAmount")
    private double ticketAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalAmount")
    private double totalAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amountPaid")
    private double amountPaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "balance")
    private double balance;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "statusId")
    private int statusId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "verifiiedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date verifiiedAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "verifiedBy")
    private int verifiedBy;

    public TicketPurchases() {
    }

    public TicketPurchases(Integer id) {
        this.id = id;
    }

    public TicketPurchases(Integer id, int purchaseNumber, int eventsId, int eventTicketsId, int consumerId, int numberOfTickets, double ticketAmount, double totalAmount, double amountPaid, double balance, Date createdAt, int statusId, Date verifiiedAt, int verifiedBy) {
        this.id = id;
        this.purchaseNumber = purchaseNumber;
        this.eventsId = eventsId;
        this.eventTicketsId = eventTicketsId;
        this.consumerId = consumerId;
        this.numberOfTickets = numberOfTickets;
        this.ticketAmount = ticketAmount;
        this.totalAmount = totalAmount;
        this.amountPaid = amountPaid;
        this.balance = balance;
        this.createdAt = createdAt;
        this.statusId = statusId;
        this.verifiiedAt = verifiiedAt;
        this.verifiedBy = verifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getPurchaseNumber() {
        return purchaseNumber;
    }

    public void setPurchaseNumber(int purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public int getEventsId() {
        return eventsId;
    }

    public void setEventsId(int eventsId) {
        this.eventsId = eventsId;
    }

    public int getEventTicketsId() {
        return eventTicketsId;
    }

    public void setEventTicketsId(int eventTicketsId) {
        this.eventTicketsId = eventTicketsId;
    }

    public int getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(int consumerId) {
        this.consumerId = consumerId;
    }

    public int getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(int numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }

    public double getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(double ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public Date getVerifiiedAt() {
        return verifiiedAt;
    }

    public void setVerifiiedAt(Date verifiiedAt) {
        this.verifiiedAt = verifiiedAt;
    }

    public int getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(int verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TicketPurchases)) {
            return false;
        }
        TicketPurchases other = (TicketPurchases) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.TicketPurchases[ id=" + id + " ]";
    }
    
}
