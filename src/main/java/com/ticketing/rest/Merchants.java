/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "merchants")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Merchants.findAll", query = "SELECT m FROM Merchants m")
    , @NamedQuery(name = "Merchants.findById", query = "SELECT m FROM Merchants m WHERE m.id = :id")
    , @NamedQuery(name = "Merchants.findByBusinessName", query = "SELECT m FROM Merchants m WHERE m.businessName = :businessName")
    , @NamedQuery(name = "Merchants.findByEmail", query = "SELECT m FROM Merchants m WHERE m.email = :email")
    , @NamedQuery(name = "Merchants.findByBusinessAddress", query = "SELECT m FROM Merchants m WHERE m.businessAddress = :businessAddress")
    , @NamedQuery(name = "Merchants.findByTown", query = "SELECT m FROM Merchants m WHERE m.town = :town")
    , @NamedQuery(name = "Merchants.findByPhoneNumber", query = "SELECT m FROM Merchants m WHERE m.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "Merchants.findByBusinessUrl", query = "SELECT m FROM Merchants m WHERE m.businessUrl = :businessUrl")
    , @NamedQuery(name = "Merchants.findByContactPersonFirstName", query = "SELECT m FROM Merchants m WHERE m.contactPersonFirstName = :contactPersonFirstName")
    , @NamedQuery(name = "Merchants.findByContactPersonMiddleName", query = "SELECT m FROM Merchants m WHERE m.contactPersonMiddleName = :contactPersonMiddleName")
    , @NamedQuery(name = "Merchants.findByContactPersonLastName", query = "SELECT m FROM Merchants m WHERE m.contactPersonLastName = :contactPersonLastName")
    , @NamedQuery(name = "Merchants.findByContactPersonPhoneNumber", query = "SELECT m FROM Merchants m WHERE m.contactPersonPhoneNumber = :contactPersonPhoneNumber")
    , @NamedQuery(name = "Merchants.findByContactPersonPosition", query = "SELECT m FROM Merchants m WHERE m.contactPersonPosition = :contactPersonPosition")
    , @NamedQuery(name = "Merchants.findByContactPersonEmail", query = "SELECT m FROM Merchants m WHERE m.contactPersonEmail = :contactPersonEmail")
    , @NamedQuery(name = "Merchants.findByCreatedAt", query = "SELECT m FROM Merchants m WHERE m.createdAt = :createdAt")
    , @NamedQuery(name = "Merchants.findByVerifiedAt", query = "SELECT m FROM Merchants m WHERE m.verifiedAt = :verifiedAt")
    , @NamedQuery(name = "Merchants.findByRemarks", query = "SELECT m FROM Merchants m WHERE m.remarks = :remarks")})
public class Merchants implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "businessName")
    private String businessName;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "businessAddress")
    private String businessAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "town")
    private String town;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Size(max = 100)
    @Column(name = "businessUrl")
    private String businessUrl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "contactPersonFirstName")
    private String contactPersonFirstName;
    @Size(max = 50)
    @Column(name = "contactPersonMiddleName")
    private String contactPersonMiddleName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "contactPersonLastName")
    private String contactPersonLastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "contactPersonPhoneNumber")
    private String contactPersonPhoneNumber;
    @Size(max = 50)
    @Column(name = "contactPersonPosition")
    private String contactPersonPosition;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "contactPersonEmail")
    private String contactPersonEmail;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "verifiedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date verifiedAt;
    @Size(max = 100)
    @Column(name = "remarks")
    private String remarks;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "merchantId")
    private Collection<EventTickets> eventTicketsCollection;
    @OneToMany(mappedBy = "merchantId")
    private Collection<Users> usersCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "merchantId")
    private Collection<SubEvents> subEventsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "merchantId")
    private Collection<EventGallery> eventGalleryCollection;
    @JoinColumn(name = "statusId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;
    @JoinColumn(name = "verifiedBy", referencedColumnName = "id")
    @ManyToOne
    private Users verifiedBy;
    @JoinColumn(name = "countryId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Country countryId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "merchantId")
    private Collection<EventSponsor> eventSponsorCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "merchantId")
    private Collection<Events> eventsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "merchantId")
    private Collection<TicketName> ticketNameCollection;

    public Merchants() {
    }

    public Merchants(Integer id) {
        this.id = id;
    }

    public Merchants(Integer id, String businessName, String email, String businessAddress, String town, String phoneNumber, String contactPersonFirstName, String contactPersonLastName, String contactPersonPhoneNumber, String contactPersonEmail, Date createdAt) {
        this.id = id;
        this.businessName = businessName;
        this.email = email;
        this.businessAddress = businessAddress;
        this.town = town;
        this.phoneNumber = phoneNumber;
        this.contactPersonFirstName = contactPersonFirstName;
        this.contactPersonLastName = contactPersonLastName;
        this.contactPersonPhoneNumber = contactPersonPhoneNumber;
        this.contactPersonEmail = contactPersonEmail;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBusinessUrl() {
        return businessUrl;
    }

    public void setBusinessUrl(String businessUrl) {
        this.businessUrl = businessUrl;
    }

    public String getContactPersonFirstName() {
        return contactPersonFirstName;
    }

    public void setContactPersonFirstName(String contactPersonFirstName) {
        this.contactPersonFirstName = contactPersonFirstName;
    }

    public String getContactPersonMiddleName() {
        return contactPersonMiddleName;
    }

    public void setContactPersonMiddleName(String contactPersonMiddleName) {
        this.contactPersonMiddleName = contactPersonMiddleName;
    }

    public String getContactPersonLastName() {
        return contactPersonLastName;
    }

    public void setContactPersonLastName(String contactPersonLastName) {
        this.contactPersonLastName = contactPersonLastName;
    }

    public String getContactPersonPhoneNumber() {
        return contactPersonPhoneNumber;
    }

    public void setContactPersonPhoneNumber(String contactPersonPhoneNumber) {
        this.contactPersonPhoneNumber = contactPersonPhoneNumber;
    }

    public String getContactPersonPosition() {
        return contactPersonPosition;
    }

    public void setContactPersonPosition(String contactPersonPosition) {
        this.contactPersonPosition = contactPersonPosition;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getVerifiedAt() {
        return verifiedAt;
    }

    public void setVerifiedAt(Date verifiedAt) {
        this.verifiedAt = verifiedAt;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @XmlTransient
    public Collection<EventTickets> getEventTicketsCollection() {
        return eventTicketsCollection;
    }

    public void setEventTicketsCollection(Collection<EventTickets> eventTicketsCollection) {
        this.eventTicketsCollection = eventTicketsCollection;
    }

    @XmlTransient
    public Collection<Users> getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Collection<Users> usersCollection) {
        this.usersCollection = usersCollection;
    }

    @XmlTransient
    public Collection<SubEvents> getSubEventsCollection() {
        return subEventsCollection;
    }

    public void setSubEventsCollection(Collection<SubEvents> subEventsCollection) {
        this.subEventsCollection = subEventsCollection;
    }

    @XmlTransient
    public Collection<EventGallery> getEventGalleryCollection() {
        return eventGalleryCollection;
    }

    public void setEventGalleryCollection(Collection<EventGallery> eventGalleryCollection) {
        this.eventGalleryCollection = eventGalleryCollection;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    public Users getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(Users verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public Country getCountryId() {
        return countryId;
    }

    public void setCountryId(Country countryId) {
        this.countryId = countryId;
    }

    @XmlTransient
    public Collection<EventSponsor> getEventSponsorCollection() {
        return eventSponsorCollection;
    }

    public void setEventSponsorCollection(Collection<EventSponsor> eventSponsorCollection) {
        this.eventSponsorCollection = eventSponsorCollection;
    }

    @XmlTransient
    public Collection<Events> getEventsCollection() {
        return eventsCollection;
    }

    public void setEventsCollection(Collection<Events> eventsCollection) {
        this.eventsCollection = eventsCollection;
    }

    @XmlTransient
    public Collection<TicketName> getTicketNameCollection() {
        return ticketNameCollection;
    }

    public void setTicketNameCollection(Collection<TicketName> ticketNameCollection) {
        this.ticketNameCollection = ticketNameCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Merchants)) {
            return false;
        }
        Merchants other = (Merchants) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.Merchants[ id=" + id + " ]";
    }
    
}
