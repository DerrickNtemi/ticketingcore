/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "communication")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Communication.findAll", query = "SELECT c FROM Communication c")
    , @NamedQuery(name = "Communication.findById", query = "SELECT c FROM Communication c WHERE c.id = :id")
    , @NamedQuery(name = "Communication.findByReceiver", query = "SELECT c FROM Communication c WHERE c.receiver = :receiver")
    , @NamedQuery(name = "Communication.findByCommunicationTypeCode", query = "SELECT c FROM Communication c WHERE c.communicationTypeCode = :communicationTypeCode")
    , @NamedQuery(name = "Communication.findBySender", query = "SELECT c FROM Communication c WHERE c.sender = :sender")
    , @NamedQuery(name = "Communication.findByCreatedAt", query = "SELECT c FROM Communication c WHERE c.createdAt = :createdAt")
    , @NamedQuery(name = "Communication.findBySendAt", query = "SELECT c FROM Communication c WHERE c.sendAt = :sendAt")
    , @NamedQuery(name = "Communication.findByUsersId", query = "SELECT c FROM Communication c WHERE c.usersId = :usersId")
    , @NamedQuery(name = "Communication.findBySubject", query = "SELECT c FROM Communication c WHERE c.subject = :subject")})
public class Communication implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "receiver")
    private String receiver;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "communicationTypeCode")
    private String communicationTypeCode;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "sender")
    private String sender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "sendAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendAt;
    @Lob
    @Size(max = 65535)
    @Column(name = "deliveryReport")
    private String deliveryReport;
    @Column(name = "usersId")
    private Integer usersId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "subject")
    private String subject;
    @JoinColumn(name = "statusId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;

    public Communication() {
    }

    public Communication(Integer id) {
        this.id = id;
    }

    public Communication(Integer id, String receiver, String communicationTypeCode, String message, String sender, Date createdAt, String subject) {
        this.id = id;
        this.receiver = receiver;
        this.communicationTypeCode = communicationTypeCode;
        this.message = message;
        this.sender = sender;
        this.createdAt = createdAt;
        this.subject = subject;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getCommunicationTypeCode() {
        return communicationTypeCode;
    }

    public void setCommunicationTypeCode(String communicationTypeCode) {
        this.communicationTypeCode = communicationTypeCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getSendAt() {
        return sendAt;
    }

    public void setSendAt(Date sendAt) {
        this.sendAt = sendAt;
    }

    public String getDeliveryReport() {
        return deliveryReport;
    }

    public void setDeliveryReport(String deliveryReport) {
        this.deliveryReport = deliveryReport;
    }

    public Integer getUsersId() {
        return usersId;
    }

    public void setUsersId(Integer usersId) {
        this.usersId = usersId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Communication)) {
            return false;
        }
        Communication other = (Communication) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.Communication[ id=" + id + " ]";
    }
    
}
