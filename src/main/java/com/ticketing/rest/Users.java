/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "users")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Users.findAll", query = "SELECT u FROM Users u")
    , @NamedQuery(name = "Users.findById", query = "SELECT u FROM Users u WHERE u.id = :id")
    , @NamedQuery(name = "Users.findByName", query = "SELECT u FROM Users u WHERE u.name = :name")
    , @NamedQuery(name = "Users.findByEmail", query = "SELECT u FROM Users u WHERE u.email = :email")
    , @NamedQuery(name = "Users.findByPhoneNumber", query = "SELECT u FROM Users u WHERE u.phoneNumber = :phoneNumber")
    , @NamedQuery(name = "Users.findByPassword", query = "SELECT u FROM Users u WHERE u.password = :password")
    , @NamedQuery(name = "Users.findByCreatedAt", query = "SELECT u FROM Users u WHERE u.createdAt = :createdAt")
    , @NamedQuery(name = "Users.findByCreatedBy", query = "SELECT u FROM Users u WHERE u.createdBy = :createdBy")
    , @NamedQuery(name = "Users.findByVerifiedAt", query = "SELECT u FROM Users u WHERE u.verifiedAt = :verifiedAt")
    , @NamedQuery(name = "Users.findByLastLogin", query = "SELECT u FROM Users u WHERE u.lastLogin = :lastLogin")
    , @NamedQuery(name = "Users.findByLoginAttempts", query = "SELECT u FROM Users u WHERE u.loginAttempts = :loginAttempts")})
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "name")
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "createdBy")
    private Integer createdBy;
    @Column(name = "verifiedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date verifiedAt;
    @Column(name = "lastLogin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Column(name = "loginAttempts")
    private Integer loginAttempts;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy")
    private Collection<EventTickets> eventTicketsCollection;
    @JoinColumn(name = "userTypesId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private UserTypes userTypesId;
    @JoinColumn(name = "userRolesId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private UserRoles userRolesId;
    @JoinColumn(name = "statusId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;
    @JoinColumn(name = "merchantId", referencedColumnName = "id")
    @ManyToOne
    private Merchants merchantId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usersId")
    private Collection<Token> tokenCollection;
    @OneToMany(mappedBy = "createdBy")
    private Collection<Token> tokenCollection1;
    @OneToMany(mappedBy = "verifiedBy")
    private Collection<Token> tokenCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<WalletCreation> walletCreationCollection;
    @OneToMany(mappedBy = "verifiedBy")
    private Collection<Merchants> merchantsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy")
    private Collection<Events> eventsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy")
    private Collection<TicketName> ticketNameCollection;

    public Users() {
    }

    public Users(Integer id) {
        this.id = id;
    }

    public Users(Integer id, String name, String email, String phoneNumber, String password, Date createdAt) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getVerifiedAt() {
        return verifiedAt;
    }

    public void setVerifiedAt(Date verifiedAt) {
        this.verifiedAt = verifiedAt;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    @XmlTransient
    public Collection<EventTickets> getEventTicketsCollection() {
        return eventTicketsCollection;
    }

    public void setEventTicketsCollection(Collection<EventTickets> eventTicketsCollection) {
        this.eventTicketsCollection = eventTicketsCollection;
    }

    public UserTypes getUserTypesId() {
        return userTypesId;
    }

    public void setUserTypesId(UserTypes userTypesId) {
        this.userTypesId = userTypesId;
    }

    public UserRoles getUserRolesId() {
        return userRolesId;
    }

    public void setUserRolesId(UserRoles userRolesId) {
        this.userRolesId = userRolesId;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    public Merchants getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchants merchantId) {
        this.merchantId = merchantId;
    }

    @XmlTransient
    public Collection<Token> getTokenCollection() {
        return tokenCollection;
    }

    public void setTokenCollection(Collection<Token> tokenCollection) {
        this.tokenCollection = tokenCollection;
    }

    @XmlTransient
    public Collection<Token> getTokenCollection1() {
        return tokenCollection1;
    }

    public void setTokenCollection1(Collection<Token> tokenCollection1) {
        this.tokenCollection1 = tokenCollection1;
    }

    @XmlTransient
    public Collection<Token> getTokenCollection2() {
        return tokenCollection2;
    }

    public void setTokenCollection2(Collection<Token> tokenCollection2) {
        this.tokenCollection2 = tokenCollection2;
    }

    @XmlTransient
    public Collection<WalletCreation> getWalletCreationCollection() {
        return walletCreationCollection;
    }

    public void setWalletCreationCollection(Collection<WalletCreation> walletCreationCollection) {
        this.walletCreationCollection = walletCreationCollection;
    }

    @XmlTransient
    public Collection<Merchants> getMerchantsCollection() {
        return merchantsCollection;
    }

    public void setMerchantsCollection(Collection<Merchants> merchantsCollection) {
        this.merchantsCollection = merchantsCollection;
    }

    @XmlTransient
    public Collection<Events> getEventsCollection() {
        return eventsCollection;
    }

    public void setEventsCollection(Collection<Events> eventsCollection) {
        this.eventsCollection = eventsCollection;
    }

    @XmlTransient
    public Collection<TicketName> getTicketNameCollection() {
        return ticketNameCollection;
    }

    public void setTicketNameCollection(Collection<TicketName> ticketNameCollection) {
        this.ticketNameCollection = ticketNameCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users)) {
            return false;
        }
        Users other = (Users) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.Users[ id=" + id + " ]";
    }
    
}
