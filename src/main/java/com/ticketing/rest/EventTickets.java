/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "eventTickets")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventTickets.findAll", query = "SELECT e FROM EventTickets e")
    , @NamedQuery(name = "EventTickets.findById", query = "SELECT e FROM EventTickets e WHERE e.id = :id")
    , @NamedQuery(name = "EventTickets.findByPrice", query = "SELECT e FROM EventTickets e WHERE e.price = :price")
    , @NamedQuery(name = "EventTickets.findByValidTill", query = "SELECT e FROM EventTickets e WHERE e.validTill = :validTill")
    , @NamedQuery(name = "EventTickets.findByTicketQuantity", query = "SELECT e FROM EventTickets e WHERE e.ticketQuantity = :ticketQuantity")
    , @NamedQuery(name = "EventTickets.findByRemainingTickets", query = "SELECT e FROM EventTickets e WHERE e.remainingTickets = :remainingTickets")
    , @NamedQuery(name = "EventTickets.findByCreatedAt", query = "SELECT e FROM EventTickets e WHERE e.createdAt = :createdAt")
    , @NamedQuery(name = "EventTickets.findByPurchaseStartDateTime", query = "SELECT e FROM EventTickets e WHERE e.purchaseStartDateTime = :purchaseStartDateTime")
    , @NamedQuery(name = "EventTickets.findByPurchaseEndDateTime", query = "SELECT e FROM EventTickets e WHERE e.purchaseEndDateTime = :purchaseEndDateTime")})
public class EventTickets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Column(name = "validTill")
    @Temporal(TemporalType.TIMESTAMP)
    private Date validTill;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ticketQuantity")
    private int ticketQuantity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "remainingTickets")
    private int remainingTickets;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "purchaseStartDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date purchaseStartDateTime;
    @Column(name = "purchaseEndDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date purchaseEndDateTime;
    @JoinColumn(name = "eventsId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Events eventsId;
    @JoinColumn(name = "ticketTypeId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TicketTypes ticketTypeId;
    @JoinColumn(name = "ticketNameId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TicketName ticketNameId;
    @JoinColumn(name = "statusId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;
    @JoinColumn(name = "createdBy", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users createdBy;
    @JoinColumn(name = "merchantId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Merchants merchantId;

    public EventTickets() {
    }

    public EventTickets(Integer id) {
        this.id = id;
    }

    public EventTickets(Integer id, int ticketQuantity, int remainingTickets, Date createdAt) {
        this.id = id;
        this.ticketQuantity = ticketQuantity;
        this.remainingTickets = remainingTickets;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getValidTill() {
        return validTill;
    }

    public void setValidTill(Date validTill) {
        this.validTill = validTill;
    }

    public int getTicketQuantity() {
        return ticketQuantity;
    }

    public void setTicketQuantity(int ticketQuantity) {
        this.ticketQuantity = ticketQuantity;
    }

    public int getRemainingTickets() {
        return remainingTickets;
    }

    public void setRemainingTickets(int remainingTickets) {
        this.remainingTickets = remainingTickets;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getPurchaseStartDateTime() {
        return purchaseStartDateTime;
    }

    public void setPurchaseStartDateTime(Date purchaseStartDateTime) {
        this.purchaseStartDateTime = purchaseStartDateTime;
    }

    public Date getPurchaseEndDateTime() {
        return purchaseEndDateTime;
    }

    public void setPurchaseEndDateTime(Date purchaseEndDateTime) {
        this.purchaseEndDateTime = purchaseEndDateTime;
    }

    public Events getEventsId() {
        return eventsId;
    }

    public void setEventsId(Events eventsId) {
        this.eventsId = eventsId;
    }

    public TicketTypes getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(TicketTypes ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public TicketName getTicketNameId() {
        return ticketNameId;
    }

    public void setTicketNameId(TicketName ticketNameId) {
        this.ticketNameId = ticketNameId;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    public Users getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Users createdBy) {
        this.createdBy = createdBy;
    }

    public Merchants getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchants merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventTickets)) {
            return false;
        }
        EventTickets other = (EventTickets) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.EventTickets[ id=" + id + " ]";
    }
    
}
