/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.rest;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author pc
 */
@Entity
@Table(name = "walletCreation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WalletCreation.findAll", query = "SELECT w FROM WalletCreation w")
    , @NamedQuery(name = "WalletCreation.findById", query = "SELECT w FROM WalletCreation w WHERE w.id = :id")
    , @NamedQuery(name = "WalletCreation.findByResponse", query = "SELECT w FROM WalletCreation w WHERE w.response = :response")
    , @NamedQuery(name = "WalletCreation.findByCreatedAt", query = "SELECT w FROM WalletCreation w WHERE w.createdAt = :createdAt")
    , @NamedQuery(name = "WalletCreation.findBySendAt", query = "SELECT w FROM WalletCreation w WHERE w.sendAt = :sendAt")})
public class WalletCreation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 100)
    @Column(name = "response")
    private String response;
    @Basic(optional = false)
    @NotNull
    @Column(name = "createdAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Column(name = "sendAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendAt;
    @JoinColumn(name = "userId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users userId;
    @JoinColumn(name = "statusId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Status statusId;

    public WalletCreation() {
    }

    public WalletCreation(Integer id) {
        this.id = id;
    }

    public WalletCreation(Integer id, Date createdAt) {
        this.id = id;
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getSendAt() {
        return sendAt;
    }

    public void setSendAt(Date sendAt) {
        this.sendAt = sendAt;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WalletCreation)) {
            return false;
        }
        WalletCreation other = (WalletCreation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ticketing.rest.WalletCreation[ id=" + id + " ]";
    }
    
}
