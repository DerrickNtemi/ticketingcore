/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.custom;

/**
 *
 * @author pc
 */
public final class MyConstants {
    
    //COMMUNICATION SENDER
    public static final String COMMUNICATION_SENDER_ADMINISTRATOR = "Administrator";
    
    //COMMUNICATION TYPE
    public static final String COMMUNICATION_TYPE_EMAIL = "CT_E";
    public static final String COMMUNICATION_TYPE_SMS = "CT_S";
    
    //TOKEN USE
    public static final String TOKEN_PURPOSE_ACCOUNTS = "TP_AA";
    
    //USER ROLES
    public static final int USER_ROLE_CLIENT = 1;
    public static final int USER_ROLE_MERCHANT_SUPER_ADMIN = 2;
    public static final int USER_ROLE_MERCHANT_USER = 3;
    public static final int USER_ROLE_SUPER_ADMIN = 4;
    public static final int USER_ROLE_ADMINISTRATOR = 5;
    
    //USER ACCOUNT TYPES
    public static final int USER_ACCOUNT_TYPE_CLIENT = 1;
    public static final int USER_ACCOUNT_TYPE_MERCHANT = 2;
    public static final int USER_ACCOUNT_TYPE_ADMINISTRATOR = 3;
    
    //APP KEYS
    public static final String APP_KEY_CLIENT = "C2C5C354B16A2EC1F4CF877C28F85";
    public static final String APP_KEY_MERCHANT = "498556A6ACE35924D962FD5BF4448";
    public static final String APP_KEY_ADMINISTRATOR = "AF6F7C3344521EA176A7A192A63FA";
    
    //STATUS
    public static final int STATUS_ACTIVE = 1;
    public static final int STATUS_CREATED = 2;
    public static final int STATUS_CANCELLED= 3;
    public static final int STATUS_PENDING = 4;
    public static final int STATUS_BLOCKED = 5;
    public static final int STATUS_VERIFIED = 6;
    public static final int STATUS_PENDING_PUBLICATION = 7;
    public static final int STATUS_DELETED = 8;
    public static final int STATUS_SENT = 9;
    public static final int STATUS_PUBLISHED = 10;
    public static final int STATUS_REJECTED = 11;

    //RESPONSE CODES
    public static final int RESPONSE_OK_STATUS = 100;
    public static final int RESPONSE_FAILED_STATUS = 101;
    public static final int RESPONSE_EMPTY_FIELD_STATUS = 102;
    public static final int RESPONSE_EXIST_STATUS = 103;
    public static final int RESPONSE_NOT_FOUND_STATUS = 104;
    public static final int RESPONSE_PROCESS_NOT_ALLOWED_STATUS = 106;
    public static final int RESPONSE_ACCESS_DENIED_STATUS = 107;
    public static final int RESPONSE_TOKEN_USED_STATUS = 109;
    public static final int RESPONSE_PASSWORD_DOES_NOT_MATCH_STATUS = 110;
    public static final int RESPONSE_TICKET_QTY_STATUS = 111;

    //RESPONSE MESSAGES
    public static final String RESPONSE_TICKET_QTY_MSG = "New quantity cannot be less than bought tickets";
    public static final String RESPONSE_OK_MESSAGE = "Operation successful";
    public static final String RESPONSE_NOT_FOUND_MESSAGE = "Record does not exist";
    public static final String RESPONSE_FAILED_MESSAGE = "Operation not successful";
    public static final String RESPONSE_EMPTY_FIELD_MESSAGE = "Mandatory fields cannot be blank";
    public static final String RESPONSE_EXIST_MESSAGE = "Record Exist";
    public static final String RESPONSE_PROCESS_NOT_ALLOWED_MSG = "Process not allowed";
    public static final String RESPONSE_ACCESS_DENIED_MSG = "Access denied";
    public static final String RESPONSE_ACCOUNT_ACCESS_MSG = "Account access denied";
    public static final String RESPONSE_TOKEN_USED_MSG = "Token already used";
    public static final String RESPONSE_PASSWORD_USERNAME_DOES_NOT_MATCH_MSG = "Username / password does not match";
    public static final String RESPONSE_PASSWORD_DOES_NOT_MATCH_MSG = "Your current password does not match";
    public static final String RESPONSE_USERNAME_DOES_NOT_EXIST_MSG = "Username does not exist";

    //PAYMENTS / INVOICE DESCRIPTIONS
    public static final String INVOICE_BOOKING_RESERVATION = "Car Rental";
    public static final String INVOICE_BOOKING_RESERVATION_CHAUFFEUR = "Chauffeur";
    public static final String INVOICE_LATE_CHECKING= "Late Check-in";

    //COMMUNICATION SENDER
    public static final String COMM_SENDER_OPERATIONS = "Operations";
    public static final String COMM_SENDER_FINANCE = "Finance";
    public static final String COMM_SENDER_ADMINSTRATOR = "Administrator";

    //COMMUNICATION EMAIL/SMS HEADERS
    public static final String COMMUNICATION_TYPE_ACCOUNT_CREATION = "CT_AC_CREATION";
    public static final String COMMUNICATION_TYPE_PASSWORD_RESET_REQUEST = "CT_PR_REQUEST";
    public static final String COMMUNICATION_TYPE_ACCOUNT_ACTIVATION = "CT_AC_ACTIVATION";
    public static final String COMMUNICATION_TYPE_MERCHANT_VERIFICATION = "CT_MER_VERIFICATION";
    public static final String COMMUNICATION_TYPE_MERCHANT_CREATION = "CT_MER_CREATION";
    public static final String COMMUNICATION_TYPE_MERCHANT_DELETING = "CT_MER_DEL";
    public static final String COMMUNICATION_TYPE_MERCHANT_UPDATING = "CT_MER_UPDATING";
    
    //EMAIL SUBJECTS
    public static final String PASSWORD_RESET_REQUEST = "Password Reset Request";
    public static final String ADMINISTRATOR_USERS_ACCOUNT_CREATION_SUBJECT = "Account Creation Confirmation";
    public static final String USER_ACCOUNT_ACTIVATION_CONFIRMATION_SUBJECT = "Account Activation Confirmation";
    public static final String MER_VERIFICATION_SBJ = "Merchant Account Verification";
    public static final String MER_UPDATING_SBJ = "Merchant Account Updating";
    public static final String MER_DEL_SBJ = "Merchant Account Suspension";
    public static final String MER_CREATION_SBJ = "Merchant Creation";
    public static final String INVOICE_SUBJECT = "Invoice No. ";
    public static final String INVOICE_CANCELLATION_SUBJECT = "Invoice Cancellation ";
    public static final String INVOICE_PAYMENT_SUBJECT = "Payment Confirmation, Invoice Ref. ";
}
