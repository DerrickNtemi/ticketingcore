/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.custom;

/**
 *
 * @author pc
 */
public class MyResponse {

    private MyStatus status;
    private String item;

    public MyResponse(MyStatus status, String item) {
        this.status = status;
        this.item = item;
    }

    public MyStatus getStatus() {
        return status;
    }

    public void setStatus(MyStatus status) {
        this.status = status;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

}
