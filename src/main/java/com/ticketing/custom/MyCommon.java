/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.custom;

import com.ticketing.rest.Status;
import com.ticketing.rest.UserRoles;
import com.ticketing.rest.UserTypes;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.core.HttpHeaders;

/**
 *
 * @author pc
 */
public class MyCommon {

    public Status getStatus(int status) {
        Status entity = new Status();
        entity.setId(status);
        return entity;
    }

    public UserRoles getRole(int role) {
        UserRoles entity = new UserRoles();
        entity.setId(role);
        return entity;
    }

    public UserTypes getUserTypes(int type) {
        UserTypes entity = new UserTypes();
        entity.setId(type);
        return entity;
    }
    
    //get current date and time and convert in int
    public int getCurrentTime() {
        return (int) (new Date().getTime() / 1000);
    }

    public Date myDate() {
        return new Date();
    }

    public String dateToString(Date d) {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(d);
        return date;
    }

    public Date next24Hours() {
        return new Date(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
    }

    public int days(Date startDate, Date endDate) {
        return (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
    }

    public String getHeaderKey(HttpHeaders headers, String key) {
        if (headers.getRequestHeader(key).size() > 0) {
            return headers.getRequestHeader(key).get(0);
        }
        return "";
    }

    public Boolean validateAppAccess(int accountType, String appKey) {
        if ((accountType == MyConstants.USER_ACCOUNT_TYPE_ADMINISTRATOR) && appKey.equals(MyConstants.APP_KEY_ADMINISTRATOR)) {
            return Boolean.TRUE;
        } else if ((accountType == MyConstants.USER_ACCOUNT_TYPE_MERCHANT) && appKey.equals(MyConstants.APP_KEY_MERCHANT)) {
            return Boolean.TRUE;
        } else if ((accountType == MyConstants.USER_ACCOUNT_TYPE_CLIENT) && appKey.equals(MyConstants.APP_KEY_CLIENT)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public Date strToDateTimeConvert(String d) {
        String expectedPattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        Date date = null;
        try {
            String userInput = d;
            date = formatter.parse(userInput);
        } catch (ParseException e) {
        }
        return date;
    }

    public Date strToDateConvert(String d) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = formatter.parse(d);
        } catch (ParseException e) {
        }
        return date;
    }

    public Date timeFormat(String time) {
        DateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
        }
        return date;
    }

    public Date createDateTime(String date, String time) {
        String dateTime = date + " " + time;
        return this.strToDateTimeConvert(dateTime);
    }

    public double formatDecimal(double number) {
        return number;
    }

}
