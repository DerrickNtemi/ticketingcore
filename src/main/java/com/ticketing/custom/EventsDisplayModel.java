/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.custom;

import com.ticketing.rest.EventGallery;
import com.ticketing.rest.EventSponsor;
import com.ticketing.rest.EventTickets;
import com.ticketing.rest.Events;
import java.util.List;

/**
 *
 * @author pc
 */
public class EventsDisplayModel {

    public Events event;
    public List<EventTickets> tickets;
    public List<EventGallery> gallery;
    public List<EventSponsor> sponsors;

    public Events getEvent() {
        return event;
    }

    public void setEvent(Events event) {
        this.event = event;
    }

    public List<EventTickets> getTickets() {
        return tickets;
    }

    public void setTickets(List<EventTickets> tickets) {
        this.tickets = tickets;
    }

    public List<EventGallery> getGallery() {
        return gallery;
    }

    public void setGallery(List<EventGallery> gallery) {
        this.gallery = gallery;
    }

    public List<EventSponsor> getSponsors() {
        return sponsors;
    }

    public void setSponsors(List<EventSponsor> sponsors) {
        this.sponsors = sponsors;
    }

}
