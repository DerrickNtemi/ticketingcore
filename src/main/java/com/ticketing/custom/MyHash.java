/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.custom;

import java.util.UUID;

/**
 *
 * @author pc
 */
public class MyHash {

    public String hashData(String originalPassword) {
        return BCrypt.hashpw(originalPassword, BCrypt.gensalt(12));
    }

    public boolean compareHash(String originalPassword, String systemPassword) {
        return BCrypt.checkpw(originalPassword, systemPassword);
    }

    public String generateToken() {
        return UUID.randomUUID().toString();
    }
}
