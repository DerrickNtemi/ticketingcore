/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.custom;

import com.ticketing.rest.Events;
import com.ticketing.rest.TicketName;
import com.ticketing.rest.TicketTypes;
import com.ticketing.rest.Users;

/**
 *
 * @author pc
 */
public class EventTicketModel {
    public int id;
    public Events eventsId;
    public TicketTypes ticketTypeId;
    public TicketName ticketNameId;
    public double price;
    public int ticketQuantity;
    public String purchaseStartDateTime;
    public String purchaseEndDateTime;
    public Users createdBy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Events getEventsId() {
        return eventsId;
    }

    public void setEventsId(Events eventsId) {
        this.eventsId = eventsId;
    }

    public TicketTypes getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(TicketTypes ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }


    public TicketName getTicketNameId() {
        return ticketNameId;
    }

    public void setTicketNameId(TicketName ticketNameId) {
        this.ticketNameId = ticketNameId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getTicketQuantity() {
        return ticketQuantity;
    }

    public void setTicketQuantity(int ticketQuantity) {
        this.ticketQuantity = ticketQuantity;
    }

    public String getPurchaseStartDateTime() {
        return purchaseStartDateTime;
    }

    public void setPurchaseStartDateTime(String purchaseStartDateTime) {
        this.purchaseStartDateTime = purchaseStartDateTime;
    }

    public String getPurchaseEndDateTime() {
        return purchaseEndDateTime;
    }

    public void setPurchaseEndDateTime(String purchaseEndDateTime) {
        this.purchaseEndDateTime = purchaseEndDateTime;
    }

    public Users getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Users createdBy) {
        this.createdBy = createdBy;
    }

    
}
