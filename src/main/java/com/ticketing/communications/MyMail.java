/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.communications;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author jikara
 */
public class MyMail {

    private static final Logger LOGGER = Logger.getLogger(MyMail.class.getName());
    static Properties mailServerProperties;
    static Session getMailSession;
    static Message generateMailMessage;

    public String sendMail(String receiver, String subject, String msg, String sender) throws AddressException, MessagingException {
        Properties properties = System.getProperties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        Session session = Session.getInstance(properties);
        try {

            getMailSession = Session.getDefaultInstance(properties, null);
            generateMailMessage = new MimeMessage(getMailSession);
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
            generateMailMessage.setSubject(subject);
            generateMailMessage.setContent(msg, "text/html");
            Transport transport = getMailSession.getTransport("smtp");
            transport.connect("smtp.gmail.com", "notification@kasneb.or.ke", "&8h4uMUm");
            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
            transport.close();
            return "Email successfully send";
        } catch (MessagingException mex) {
            LOGGER.log(Level.SEVERE, "Email Error  {0}", mex);
        }
        return "";

    }
}
