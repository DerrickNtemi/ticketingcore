/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ticketing.communications;

import com.ticketing.custom.MyCommon;
import com.ticketing.custom.MyConstants;
import com.ticketing.rest.Communication;
import com.ticketing.rest.Merchants;
import com.ticketing.rest.Users;
import com.ticketing.services.CommunicationsFacade;
import com.ticketing.services.MerchantsFacade;
import com.ticketing.services.UsersFacadeREST;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.mail.MessagingException;

/**
 *
 * @author pc
 */
@Singleton
@Startup
public class CommunicationScheduler {

    private static final Logger LOGGER = Logger.getLogger(CommunicationScheduler.class.getName());

    @EJB
    CommunicationsFacade communicationsFacade;
    @EJB
    UsersFacadeREST usersFacadeREST;
    @EJB
    MerchantsFacade merchantsFacade;

    public CommunicationScheduler() {
    }

    @Schedule(dayOfWeek = "*", hour = "*", minute = "*", second = "*/10", persistent = false)
    public void checkEvent() throws MessagingException {
        MyCommon myCommon = new MyCommon();
        List<Communication> communications = communicationsFacade.findByStatus();
        for (Communication communication : communications) {
            try {
                if (communication.getCommunicationTypeCode().equals(MyConstants.COMMUNICATION_TYPE_SMS)) {//implement message sending
                    MySms ms = new MySms();
                    String smsResponse = ms.sendSMS(communication);
                    if (smsResponse.equals("200")) {
                        communication.setDeliveryReport("Sms Successfully delivered");
                        communication.setSendAt(new Date());
                        communication.setStatusId(myCommon.getStatus(MyConstants.STATUS_SENT));
                        communicationsFacade.edit(communication);
                    }
                } else if (communication.getCommunicationTypeCode().equals(MyConstants.COMMUNICATION_TYPE_EMAIL)) { //implement email sending
                    Users user;
                    Merchants merchant;
                    String templateName = communication.getSubject();
                    String subject = "";
                    String recepient = "";
                    String sender = "";
                    String message = this.messageReader(communication);
                    switch (templateName) {
                        case MyConstants.COMMUNICATION_TYPE_ACCOUNT_CREATION:
                            //user account
                            user = usersFacadeREST.find(Integer.parseInt(communication.getReceiver()));
                            subject = MyConstants.ADMINISTRATOR_USERS_ACCOUNT_CREATION_SUBJECT;
                            sender = MyConstants.COMM_SENDER_ADMINSTRATOR;
                            recepient = user.getEmail();
                            break;
                        case MyConstants.COMMUNICATION_TYPE_PASSWORD_RESET_REQUEST:
                            //user account password reset
                            user = usersFacadeREST.find(Integer.parseInt(communication.getReceiver()));
                            subject = MyConstants.PASSWORD_RESET_REQUEST;
                            sender = MyConstants.COMM_SENDER_ADMINSTRATOR;
                            recepient = user.getEmail();
                            break;
                        case MyConstants.COMMUNICATION_TYPE_ACCOUNT_ACTIVATION: {
                            //user account activation
                            user = usersFacadeREST.find(Integer.parseInt(communication.getReceiver()));
                            subject = MyConstants.USER_ACCOUNT_ACTIVATION_CONFIRMATION_SUBJECT;
                            sender = MyConstants.COMM_SENDER_ADMINSTRATOR;
                            recepient = user.getEmail();
                            break;
                        }
                        case MyConstants.COMMUNICATION_TYPE_MERCHANT_VERIFICATION: {
                            //user account activation
                            merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                            subject = MyConstants.MER_VERIFICATION_SBJ;
                            sender = MyConstants.COMM_SENDER_ADMINSTRATOR;
                            recepient = merchant.getEmail();
                            break;
                        }
                        case MyConstants.COMMUNICATION_TYPE_MERCHANT_UPDATING: {
                            //user account activation
                            merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                            subject = MyConstants.MER_UPDATING_SBJ;
                            sender = MyConstants.COMM_SENDER_ADMINSTRATOR;
                            recepient = merchant.getEmail();
                            break;
                        }
                        case MyConstants.COMMUNICATION_TYPE_MERCHANT_DELETING: {
                            //user account activation
                            merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                            subject = MyConstants.MER_DEL_SBJ;
                            sender = MyConstants.COMM_SENDER_ADMINSTRATOR;
                            recepient = merchant.getEmail();
                            break;
                        }
                        case MyConstants.COMMUNICATION_TYPE_MERCHANT_CREATION: {
                            //user account activation
                            merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                            subject = MyConstants.MER_CREATION_SBJ;
                            sender = MyConstants.COMM_SENDER_ADMINSTRATOR;
                            recepient = merchant.getEmail();
                            break;
                        }
                        default:
                            break;
                    }
                    MyMail mail = new MyMail();
                    String transport = mail.sendMail(recepient, subject, message, sender);
                    if (transport.equals("Email successfully send")) {
                        communication.setSendAt(new Date());
                        communication.setStatusId(myCommon.getStatus(MyConstants.STATUS_SENT));
                    }
                    communication.setDeliveryReport(transport);
                    communicationsFacade.edit(communication);
                }
            } catch (NumberFormatException e) {
                LOGGER.log(Level.INFO, "Email Schedular {0}", e);
            }
        }
    }

    public String messageReader(Communication communication) {
        String mail = "";
        StringBuilder builder = new StringBuilder();
        String request = "http://127.0.0.1:8080/ticketing/email?commId=" + communication.getId();
        try {
            URL url = new URL(request);
            HttpURLConnection client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("GET");
            BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            String text;
            while ((text = reader.readLine()) != null) {
                builder.append(text);
            }
            mail = builder.toString();
        } catch (IOException e) {
        }

        return mail;
    }

}
