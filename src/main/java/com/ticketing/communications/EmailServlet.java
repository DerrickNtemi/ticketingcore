package com.ticketing.communications;

import com.ticketing.custom.MyConstants;
import com.ticketing.rest.Communication;
import com.ticketing.rest.Merchants;
import com.ticketing.rest.Token;
import com.ticketing.rest.Users;
import com.ticketing.services.CommunicationsFacade;
import com.ticketing.services.MerchantsFacade;
import com.ticketing.services.TokenFacadeREST;
import com.ticketing.services.UsersFacadeREST;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/email")
public class EmailServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(EmailServlet.class.getName());

    @EJB
    CommunicationsFacade communicationFacade;
    @EJB
    UsersFacadeREST usersFacadeREST;
    @EJB
    TokenFacadeREST tokenFacadeREST;
    @EJB
    MerchantsFacade merchantsFacade;
    
    /**
     *
     * @param request
     * @param res
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse res) throws IOException, ServletException {
        Integer id = Integer.parseInt(request.getParameter("commId"));
        Communication communication = communicationFacade.find(id);
        Users user;
        Merchants merchant;
        String templateName = communication.getSubject();
        switch (templateName) {
            case MyConstants.COMMUNICATION_TYPE_ACCOUNT_CREATION: {
                //client user account template
                user = usersFacadeREST.find(Integer.parseInt(communication.getReceiver()));
                templateName = "/user.jsp";
                Token token = tokenFacadeREST.getToken(user);
                request.setAttribute("user", user);
                request.setAttribute("token", token);
                request.setAttribute("communication", communication);
                break;
            }
            case MyConstants.COMMUNICATION_TYPE_PASSWORD_RESET_REQUEST: {
                //client user account template
                user = usersFacadeREST.find(Integer.parseInt(communication.getReceiver()));
                templateName = "/passwordrequestreset.jsp";
                Token token = tokenFacadeREST.getToken(user);
                request.setAttribute("user", user);
                request.setAttribute("token", token);
                request.setAttribute("communication", communication);
                break;
            }
            case MyConstants.COMMUNICATION_TYPE_ACCOUNT_ACTIVATION: {
                //user account activation template
                templateName = "/useractivation.jsp";
                request.setAttribute("communication", communication);
                break;
            }
            case MyConstants.COMMUNICATION_TYPE_MERCHANT_CREATION: {
                //user account activation template
                merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                templateName = "/merchantcreation.jsp";
                request.setAttribute("merchant", merchant);
                request.setAttribute("communication", communication);
                break;
            }
            case MyConstants.COMMUNICATION_TYPE_MERCHANT_VERIFICATION: {
                //user account activation template
                merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                templateName = "/merchantverification.jsp";
                request.setAttribute("merchant", merchant);
                request.setAttribute("communication", communication);
                break;
            }
            case MyConstants.COMMUNICATION_TYPE_MERCHANT_UPDATING: {
                //user account activation template
                merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                templateName = "/merchantupdating.jsp";
                request.setAttribute("merchant", merchant);
                request.setAttribute("communication", communication);
                break;
            }
            case MyConstants.COMMUNICATION_TYPE_MERCHANT_DELETING: {
                //user account activation template
                merchant = merchantsFacade.find(Integer.parseInt(communication.getReceiver()));
                templateName = "/merchantdeleting.jsp";
                request.setAttribute("merchant", merchant);
                request.setAttribute("communication", communication);
                break;
            }
            default:
                break;
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(templateName);
        dispatcher.forward(request, res);
    }
}
