<%@page import="com.ticketing.custom.MyCommon"%>
<%@page import="com.ticketing.custom.MyConstants"%>
<%@page import="com.ticketing.rest.Communication"%>
<%@page import="com.ticketing.rest.Token"%>
<%@page import="com.ticketing.rest.Users"%>
<jsp:include page="header.jsp" />
<%
    MyCommon myCommon = new MyCommon();
    Users user = (Users) request.getAttribute("user");
    Token token = (Token) request.getAttribute("token");
    Communication communication = (Communication) request.getAttribute("communication");
    String activationLink = "";
    String b2 = "http://192.168.11.249/";
    if (user.getUserTypesId().getId().equals(MyConstants.USER_ACCOUNT_TYPE_CLIENT)) {
        activationLink = b2 + "merchants/site/activate-account?token=" + token.getToken();
    } else if (user.getUserTypesId().getId().equals(MyConstants.USER_ACCOUNT_TYPE_MERCHANT)) {
        activationLink = b2 + "merchants/site/activate-account?token=" + token.getToken();
    } else {
        activationLink = b2 + "administrator/site/activate-account?token=" + token.getToken();
    }
    String link = "<a href=" + activationLink + ">click to activate account<a>";
%>
<div style="width:100%; padding: 5px; margin:5px; min-height: 250px;" >
    <div style="font-size: 0.9em;">Hi <%= user.getName()%>,</div>
    <div style="margin-top: 8px; font-size: 0.9em; ">Welcome on-board. Your account was successfully created and your login details are as below:</div>
    <div style="font-size: 0.9em;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Username: <%= user.getEmail()%></div>
    <div style="font-size: 0.9em;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Activation Link: <%= link%> </div>
    <div style="margin-top: 5px; font-size: 0.9em; ">Kindly note that you will be requested to change your password on your first connection.</div>
    <div style="margin-top: 5px; font-size: 0.9em; ">Please do not hesitate to contact our team should you have any question!</div>
    <div style="margin-top: 20px; font-size: 0.9em; ">Best regards,</div>
    <div style="font-size: 0.9em; "><%= communication.getSender()%></div>
</div>
<jsp:include page="footer.jsp" />
