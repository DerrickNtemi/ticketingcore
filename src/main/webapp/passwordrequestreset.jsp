<%@page import="com.ticketing.custom.MyCommon"%>
<%@page import="com.ticketing.custom.MyConstants"%>
<%@page import="com.ticketing.rest.Communication"%>
<%@page import="com.ticketing.rest.Token"%>
<%@page import="com.ticketing.rest.Users"%>
<jsp:include page="header.jsp" />
<%
    MyCommon myCommon = new MyCommon();
    Users user = (Users)request.getAttribute("user");
    Token token = (Token)request.getAttribute("token");
    Communication communication = (Communication) request.getAttribute("communication");    
    String activationLink = "";
    String b2 = "http://192.168.11.249/";
    if (user.getUserTypesId().getId().equals(MyConstants.USER_ACCOUNT_TYPE_CLIENT)) {
        activationLink = b2 + "merchants/site/activate-account?token=" + token.getToken();
    } else if (user.getUserTypesId().getId().equals(MyConstants.USER_ACCOUNT_TYPE_MERCHANT)) {
        activationLink = b2 + "merchants/site/activate-account?token=" + token.getToken();
    } else {
        activationLink = b2 + "administrator/site/activate-account?token=" + token.getToken();
    }
    String link = "<a href=" + activationLink + ">click to activate account<a>";
    
%>
<div style="width:100%; padding: 5px; margin:5px; min-height: 250px;" >
    <div style="font-size: 0.9em;">Hi <%= user.getName() %>,</div>
    <div style="margin-top: 5px; font-size: 0.9em; ">Recently a request was submitted to reset your password. Please click the link below:</div>
    <div style="font-size: 0.9em;"><%= link %> </div>
    <div style="margin-top: 5px; font-size: 0.9em; "> If you did not request this, please ignore this email.</div>
    <div style="margin-top: 5px; font-size: 0.9em; "> Please do not hesitate to contact our team should you have any question!</div>
    <div style="margin-top: 20px; font-size: 0.9em; ">Thanks,</div>
    <div style="font-size: 0.9em; "><%= communication.getSender() %></div>
</div>
<jsp:include page="footer.jsp" />
