<%@page import="com.ticketing.rest.Communication"%>

<jsp:include page="header.jsp" />
<%
    Communication communication = (Communication) request.getAttribute("communication");
%>
<div style="width:100%; padding: 5px; margin:5px;" >
    <div style="font-size: 0.9em;" >Hi <%= communication.getMessage() %>,</div>
    <div style="margin-top: 8px; margin-bottom: 15px; font-size: 0.9em;" >Welcome to the on-board! Your account has been successfully activated.</div>
    <div style="margin-top: 8px; margin-bottom: 15px; font-size: 0.9em;" >Please do not hesitate to contact our team should you have any question!</div>
    <div style="margin-top: 50px; font-size: 0.9em;" >Regards,</div>
    <div style="font-size: 0.9em;"><%= communication.getSender()%></div>
</div>
<jsp:include page="footer.jsp" />
