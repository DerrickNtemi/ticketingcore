<%@page import="com.ticketing.rest.Communication"%>
<%@page import="com.ticketing.rest.Merchants"%>
<jsp:include page="header.jsp" />
<%
    Merchants merchant = (Merchants) request.getAttribute("merchant");
    Communication communication = (Communication) request.getAttribute("communication");
%>
<div style="width:100%; padding: 5px; margin:5px; min-height: 250px;" >
    <div style="font-size: 0.9em;">Welcome on-board <%= merchant.getBusinessName()%>,</div>
    <div style="margin-top: 5px; font-size: 0.9em; ">Please do not hesitate to contact our team should you have any question!</div>
    <div style="margin-top: 20px; font-size: 0.9em; ">Best regards,</div>
    <div style="font-size: 0.9em; "><%= communication.getSender()%></div>
</div>
<jsp:include page="footer.jsp" />